package com.visahelper.data;

import com.google.gson.annotations.SerializedName;

public class Post {
    public int userId;
    public int id;
    public String title;
    public String Country;
    public int Schengen;
    public String Filtr_time;
    public String Filtr_purpose;

    @SerializedName("body")
    private String text;

    public int getUserId() {
        return userId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCountry() {
        return Country;
    }

    public int getSchengen() {
        return Schengen;
    }

    public String getFilter_time() { return Filtr_time; }

    public String getFilter_purpose() {
        return Filtr_purpose;
    }

    public String getText() {
        return text;
    }
}
