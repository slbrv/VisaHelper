package com.visahelper.data;

import com.google.gson.annotations.SerializedName;

public class Post4 {
    public String key;

    @SerializedName("body")
    private String text;

    public String getKey() {
        return key;
    }

    public String getText() {
        return text;
    }
}
