package com.visahelper.data;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DataHolderApi {
    @GET("get_visa1.php")
    Call<List<Post>> getPost(@Query("category") String coun);

    @GET("get_dok.php")
    Call<List<Post2>> getPost2(@Query("category") String coun, @Query("category2") String vi);

    @GET("get_inf.php")
    Call<List<Post5>> getPost5(@Query("category") String coun, @Query("category2") String vi);

    @GET("get_user_public_data.php")
    Call<UserPublicData> getUserPublicData(@Query("token") String token);
}
