package com.visahelper.data;

import com.google.gson.annotations.SerializedName;

public class Post3 {
    public String answer;
    //public int customerhavedoc;
    @SerializedName("body")
    private String text;

    public String getAnswer() {
        return answer;
    }

    public String getText() {
        return text;
    }
}
