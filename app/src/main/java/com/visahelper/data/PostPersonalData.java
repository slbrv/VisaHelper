package com.visahelper.data;

import com.google.gson.annotations.SerializedName;

public class PostPersonalData {
    @SerializedName("token")
    public String mToken;
    @SerializedName("visa_id")
    public int mVisaId;
    @SerializedName("data")
    public String mData;
}
