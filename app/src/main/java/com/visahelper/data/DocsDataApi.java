package com.visahelper.data;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface DocsDataApi {

    @POST("post_personal_data.php")
    Call<Void> postPersonalData(@Body PostPersonalData data);

    @GET("get_personal_data.php")
    Call<List<String>> getPersonalData(@Query("token") String token);

    @GET("get_ank_data.php")
    Call<List<Integer>> getAnkData(@Query("token") String token, @Query("visa_id") int visaId);
}
