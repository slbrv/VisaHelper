package com.visahelper.data;

import com.google.gson.annotations.SerializedName;

public class UserPublicData {
    @SerializedName("first_name")
    public String mFirstName;
    @SerializedName("middle_name")
    public String mMiddleName;
    @SerializedName("last_name")
    public String mLastName;
}
