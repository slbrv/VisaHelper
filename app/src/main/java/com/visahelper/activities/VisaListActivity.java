package com.visahelper.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visahelper.config.Config;
import com.visahelper.R;
import com.visahelper.data.DataHolderApi;
import com.visahelper.data.Post;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.visahelper.data.UserPublicData;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * класс для формирования и вывода списка виз, доступных для оформления в выбранную страну
 * @author Nargiz
 * @version 1.0
 */
public class VisaListActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * кнопки перехода на другие страницы
     */
    private Button specialbtnto2str, enterbtn3, personalRoombtn3;
    /**
     * кнопка перехода на предыдущую страницу
     */
    private Button vBackButton;
    /**
     * кнопка выхода из системы
     */
    private Button mExitButton;
    /**
     * текстовое поля для вывода названия страны
     */
    private TextView mCountryNameField;
    /**
     * переменные для фильтров
     */
    private String typevisachoose = null, timevisachoose = null, purposevisachoose = null;
    /**
     * переменная, хранящая страну
     */
    private String mTargetCountry;
    /**
     * переменная, хранящая название визы
     */
    private String mVisaName;
    /**
     * переменная, хранящая id визы
     */
    private int mVisaId;
    /**
     * переменная, хранящая токен пользователя
     */
    private String mToken;
    /**
     * переменная, хранящая данные пользователя
     */
    private UserPublicData mUserPublicData;
    /**
     * переменные для хранения информации об авторизованности пользователя в системе
     */
    private int activationOfAccount = 0, activationOfAccount2 = 0;
    /**
     * переменная для хранения переведенного названия страны
     */
    private String mTranslitCountryName;
    /**
     * хранилище данных MutableLiveData
     */
    private MutableLiveData<String[]> visaLiveData = new MutableLiveData<>();
    /**
     * переменная хранения выводимых виз
     */
    private String[] visa = new String[230];
    /**
     * call - переменная, которая хранит возвращаемое значение типа Call
     */
    Call<List<Post>> call;
    List<Post> posts;
    int changebtn1 = 0, changebtn2 = 0, changebtn3 = 0;

    String[] purposevisa = {"Цель визы: не выбрано", "Туризм", "Работа", "Учеба", "Другое"};
    String[] typevisa = {"Тип визы: не выбрано", "Шенгенская", "Национальная"};
    String[] timevisa2 = {"Время: не выбрано", "Менее 90 дней", "Более 90 дней"};
    String[] timevisa = {"Время: не выбрано", "Краткосрочная", "Долгосрочная"};
    String infobulgary = "\t\t\t\t\t\tВизовые сборы:\n" +
            "o\tДля получения аэропортной транзитной визы (А), транзитной визы (В) - 60 EUR\n" +
            "o\tДля получения краткосрочной визы (С) гражданами России - 35 EUR. \n" +
            "Соглашение между Российской Федерацией и Европейским сообществом об упрощении выдачи виз гражданам Российской Федерации и Европейского Союза\" (заключено в г. Сочи 25.05.2006)\n" +
            "o\tДля получения краткосрочной визы (С) в срочном порядке - 70 EUR\n" +
            "o\tДля получения долгосрочной визы (D) до 180 дней по Закону об иностранных гражданах РБ, ст.15, п.1 - 100 EUR\n" +
            "o\tДля получения долгосрочной визы (D) до 360 дней по Закону об иностранных гражданах РБ, §15, п.2 - 200 EUR\n" +
            "Внимание! Оплата производится наличными в евро при подаче заявления на получение визы.\n\n" +
            "\t\t\t\t\t\t\t\t Освобождаются от уплаты \n\t\t\t\t\t\t\t\t\t\t\t\tвизового сбора:\n" +
            "1.\tБлизкие родственники - супруги, дети (в том числе приемные), родители (в том числе опекуны и попечители), бабушки и дедушки, внуки граждан РФ и ЕС, проживающих на законных основаниях на территории одного из государств-членов или территории РФ;\n" +
            "2.\tШкольники, студенты, аспиранты и сопровождающие их преподаватели, направляющиеся на учебу или учебную стажировку;\n" +
            "3.\tИнвалиды и лица, которые при необходимости их сопровождают;\n" +
            "4.\tЛица, которые представили документы, подтверждающие необходимость осуществления поездки гуманитарного характера, в том числе для получения срочной медицинской помощи, и сопровождающие их лица, для присутствия на похоронах или посещения тяжело больного близкого родственника;\n" +
            "5.\tУчастники молодежных международных спортивных мероприятий и сопровождающие их лица;\n" +
            "6.\tЛица, участвующие в научной, культурной или творческой деятельности, в том числе в университетских и других программах обмена;\n" +
            "7.\tДети до 6 лет;\n" +
            "8.\tИ другие - на основании ст.6, п.3 Соглашения между РФ и Европейским Сообществом об упрощении выдачи виз гражданам РФ и ЕС. \n" +
            "\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tПорядок получения визы:\n" +
            "Краткосрочная виза (С).\n" +
            "o\tПриём заявлений без предварительной записи только для членов семьи и/или ближайших родственников граждан государств ЕС, ЕЭС, и Швейцарии. \n" +
            "Время приёма документов: 09.00 ч. – 11.00 ч. (Понедельник-Пятница).\n" +
            "o\tВо всех остальных случаях прием заявлений - только по предварительной записи по телефону +7(499)709-9281 или +7(499)703-3748. \n" +
            "Время приёма документов: 09.00 ч. – 11.00 ч. (Понедельник-Пятница).\n" +
            "o\tЗаявление на визу подаётся не раньше, чем за 3 месяца и не позже, чем за 15 дней до начала поездки. \n" +
            "Срок рассмотрения заявления может быть увеличен, если в отношении заявления требуются дополнительные разъяснения\n" +
            "Долгосрочная Виза (С)\n" +
            "o\tПриём заявлений только по предварительной записи по телефону +7(499)709-9281 или +7(499)703-3748. Время приёма документов: 09.00 ч. – 12.00 ч. (Понедельник-Пятница).\n" +
            "o\tЗаявление подается: лично заявителем; непосредственно в Консульском отделе Посольства Республики Болгария в Москве. \n" +
            "o\tПолучение документов производится лично или по заверенной нотариально доверенности.\n" +
            "o\tМинимальный срок оформления составляет 30 рабочих дней с момента подачи документов. При необходимости срок может быть продлен до 20 дней.\n" +
            "\n" +
            "\uF0A7\tИнформация о статусе рассмотрения заявления можно посмотреть по ссылке:\n" +
            Html.fromHtml("https://www.vfsvisaservicesrussia.com/Global-Passporttracking/") + " \n" +
            "\uF0A7\tПолучение информации  и выдача готовых виз производится в порядке живой очереди с 09.00 ч. до 11.00 ч. Понедельник-Пятница.\n" +
            "\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tИнформация о посольстве \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tи визовых центрах:\n" +
            "•\tПосольство Республики Болгария в Москве\n" +
            "o\tАдрес: 119590, Москва, Мосфильмовская ул., д. 66\n" +
            "o\tТелефон: +7(499) 143-9022 (консульская служба), 143-9023\n" +
            "o\tE-Mail: bulemrus@bоlgaria.ru, консульская служба - consul@bolgaria.ru\n" +
            "o\tОфициальный веб-сайт: http://info.bolgaria.ru/index.php\n" +
            "•\tГенеральное консульство Болгарии в Санкт-Петербурге\n" +
            "o\tАдрес: 197110, Санкт-Петербург, ул. Рылеева, 27 (м. Чернышевского)\n" +
            "o\tТелефон: +7(812) 401-0152\n" +
            "o\tE-Mail: secretary@mail.wplus.net\n" +
            "•\tГенеральное консульство Болгарии в Екатеринбурге\n" +
            "o\tАдрес:  620075, Екатеринбург, ул.Гоголя, 15\n" +
            "o\tТелефон: +7(343) 282-9844\n" +
            "\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tОфициальные сервисные визовые \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tцентры Болгарии в России (VFS Global)\n" +
            "o\tТелефон: 8 (499) 709-92-81 или  8 (499) 703-37-48 с понедельника по пятницу с 9:00 до 18:00 по московскому времени (за исключением праздников). \n" +
            "o\tВремя приёма документов: Сервисные Визовые Центры Болгарии работают с понедельника по пятницу (кроме праздничных дней) с 09.00 - 16.00 часов. \n" +
            "o\t E-mail: infomosbul@vfsglobal.com\n" +
            "o\tОфициальный веб-сайт:www.vfsglobal.com/Bulgaria/Russia\n" +
            "\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tАдреса  VFS Global в России:\n" +
            "1.\tАрхангельск - пр. Ломоносова , 81\n" +
            "2.\tБарнаул - Пролетарская улица, дом 65\n" +
            "3.\tБелгород - проспект Ватутина, д.2\n" +
            "4.\tБрянск - улица Красноармейская, дом 38\n" +
            "5.\tВеликий Новгород - Сырковское шоссе, д.2 А, Бизнес Центр «Колмово Сити», 6 этаж\n" +
            "6.\tВладивосток - Океанский проспект, дом 17, офис 601\n" +
            "7.\tВладимир - ул. Разина, 22\n" +
            "8.\tВолгоград - ул. Маршала Чуйкова, дом 73\n" +
            "9.\tВологда - Пречистенская набережная 34А\n" +
            "10.\tВоронеж - ул. Ворошилова, д. 16\n" +
            "11.\tВыборг - Московский проспект, д. 9, офис 8Н\n" +
            "12.\tЕкатеринбург - ул. Куйбышева, 44 Д, 2 этаж, бизнес-отель \"Панорама\" (вход с улицы Белинского)\n" +
            "13.\tИвангород - ул. Юрия Гагарина, 7\n" +
            "14.\tИваново - улица Шубина, дом 30А\n" +
            "15.\tИжевск - Карлутская набережная, 9 офис 9\n" +
            "16.\tИркутск - ул. Свердлова, д.10, 2 этаж\n" +
            "17.\tКазань - ул. Парижской Коммуны д.8 (вход с улицы Московская)\n" +
            "18.\tКалининград - ул. 1812 года, д.126\n" +
            "19.\tКалуга - улица Воронина, дом 28\n" +
            "20.\tКемерово - улица 50 лет Октября, дом 11\n" +
            "21.\tКиров - улица Маклина, д. 37\n" +
            "22.\tКраснодар - ул. Академика Павлова 64\n" +
            "23.\tКрасноярск - ул. Маерчака, д. 16\n" +
            "24.\tКурган - улица Коли Мяготина, дом 12\n" +
            "25.\tКурск - ул. Станционная, дом 15\n" +
            "26.\tЛипецк - ул. Литаврина, д. 6а\n" +
            "27.\tМахачкала - ул. Ушакова, д.2Л\n" +
            "28.\tМосква - Каширское шоссе д. 3, к. 2, стр. 4 (Бизнес-центр \"Сириус Парк\")\n" +
            "29.\tМурманск - ул. Карла-Либкнехта, д. 13\n" +
            "30.\tНижний Новгород - ул. Щербакова, д. 15, 1 этаж\n" +
            "31.\tНовокузнецк - проспект Н.С. Ермакова, дом 3\n" +
            "32.\tНовороссийск - ул. Карла Маркса, д. 49\n" +
            "33.\tНовосибирск - ул. Челюскинцев, д.15\n" +
            "34.\tОмск - ул. Фрунзе 1, стр. 4, офис 713\n" +
            "35.\tОренбург - ул. Комсомольская/Рыбаковская ул., Д.99/41\n" +
            "36.\tПенза - улица Кулакова, дом 1\n" +
            "37.\tПермь - ул. Чернышевского, 28, 2 этаж\n" +
            "38.\tПетрозаводск - улица Гоголя, д.6\n" +
            "39.\tПсков - Рижский проспект, д.60\n" +
            "40.\tПятигорск - улица Кучуры, д.8\n" +
            "41.\tРостов на Дон - ул. Троллейбусная, д. 24/2в, 2 этаж\n" +
            "42.\tРязань - улица Лермонтова, дом 20, строение 1\n" +
            "43.\tСамара - ул. Мичурина, д.78, офис 2. (Бизнес-центр „Миллениум“)\n" +
            "44.\tСанкт Петербург - ул. Большая Разночинная, 16А (вход с Чкаловского пр. 7), 2 этаж\n" +
            "45.\tСаранск - ул. Большевистская, дом 60\n" +
            "46.\tСаратов - ул. Вавилова, 38/114, офис 2а\n" +
            "47.\tСмоленск - ул. Николаева, д. 20\n" +
            "48.\tСочи - Шоссейная улица, 8А\n" +
            "49.\tСургут - улица Маяковского, д. 57\n" +
            "50.\tТомск - ул. Кирова, д. 51А\n" +
            "51.\tТула - улица Тургеневская, дом 7\n" +
            "52.\tТюмень - ул. Малыгина 75\n" +
            "53.\tУльяновск - улица Карла Либкнехта, дом 24/5а\n" +
            "54.\tУфа - ул. Чернышевского 82, офис 301\n" +
            "55.\tХабаровск - ул. Истомина 22а, 1 этаж\n" +
            "56.\tЧелябинск - улица Карла Маркса, 38, офис 204\n" +
            "57.\tЧереповец - улица Дзержинского, дом 49\n" +
            "58.\tЧита - Амурская улица, дом 2\n" +
            "\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tНормативно правовые акты:\n" +
            "1.\tСоглашение между Российской Федерацией и Европейским сообществом об упрощении выдачи виз гражданам Российской Федерации и Европейского Союза\" (заключено в г. Сочи) http://www.consultant.ru/document/cons_doc_LAW_126335/\n" +
            "2.\tЗакон об иностранцах в Республике Болгария. https://lex.bg/laws/ldoc/2134455296\n" +
            "3.\tТАРИФА № 3 за таксите, които се събират за консулско обслужване в системата на Министерството на външните работи по Закона за държавните такси Приета с ПМС № 333 от 28.12.2007 г., обн., ДВ, бр. 3 от 11.01.2008 г., в сила от 11.01.2008 г.\n" +
            "4.\tНаредба за условията и реда за издаване на визи и определяне на визовия режим очи 25.05.2006)\n";

    String infoitaly = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tВизовые сборы:\n" +
            "o\tДля получения транзитной визы на срок пребывания 0 дней: 35 EUR\n" +
            "o\t Для получения туристической, деловой, рабочей, медицинской, спортивной, транзитной, транспортной визы, визы по религиозным мотивам на срок до 90 дней: 35 EUR\n" +
            "o\tДля получения студенческой визы на срок до 90 дней: бесплатно\n" +
            "o\tДля получения медицинской, рабочей визы, а также визы по религиозным или семейным мотивам, визы для повторного въезда, визы выбранного места жительства на срок от 91 до 365 дней: 116 EUR\n" +
            "o\tДля получения студенческой визы на срок от 91 до 365 дней: 50 EUR\n\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tВнимание! Оплата производится только наличными в рублях, долларах или евро.\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tОсвобождаются от уплаты визового сбора при подаче документов на визу типа «C»:\n" +
            "1)\tБлизкие родственники граждан Европейского Союза и Швейцарской Конфедерации (супруг, партнер, с которым гражданин EC вступил в зарегистрированное партнерство, если в соответствии с законодательством принимающего государства зарегистрированные партнерства признаются эквивалентными браку, прямые потомки, которые не достигли возраста 21 года либо находятся на иждивении, и аналогичные потомки супруга или партнера, родственники по прямой восходящей линии, находящиеся на иждивении, и аналогичные родственники супруга или партнера);\n" +
            "2)\tБлизкие родственники граждан Российской Федерации, не являющихся гражданами ЕС, но легально проживающих на территории стран Европейского Союза, т.е. имеющих Вид на Жительство на территории ЕС (за исключением Швейцарской Конфедерации): супруги, дети (в том числе приемные), родители (в том числе опекуны и попечители), дедушки, бабушки и внуки.\n" +
            "Внимание! Необходимо предоставить документы, подтверждающие родство, а также копию паспорта/ вида на жительство лица, проживающего на территории ЕС.\n" +
            "3)\tРодственники граждан Европейского Союза и Швейцарской Конфедерации;\n" +
            "4)\tНесовершеннолетние в возрасте до 6 лет на момент подачи заявления, его рассмотрения;\n" +
            "5)\tИнвалиды всех групп и одно сопровождающее лицо старше 18 лет \n" +
            "Внимание! Необходимо предоставить оригинал и копию свидетельство об инвалидности.\n" +
            "6)\tУчастники молодежных международных спортивных мероприятий, сопровождающие лица;\n" +
            "7)\tУчастники официальных программ обмена, организованных городами-побратимами.\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tПорядок получения визы:\n" +
            "o\tДокументы на оформление визы можно подать в Визовый центр не ранее чем за 90 дней до начала запланированной поездки.\n" +
            "o\tДля подачи документов необходимо записаться через онлайн-форму на сайте официального Визового центра Италии: https://italy-vms.ru/nesovershennoletnie-grazhdane/\n" +
            "o\tПрием физических лиц в Визовом центре проводится по предварительной записи Для записи оформите заявку на нашем сайте в разделе Запись на подачу документов.\n" +
            "o\tНельзя вносить данные не более четырёх человек – заявителя и трёх членов семьи. Если членов семьи больше, необходимо создавать ещё одну запись.\n" +
            "o\tДля супругов, родственников по восходящей и нисходящей линии граждан Европейского Союза не обязательно заранее записываться. Достаточно прийти в рабочие часы Генерального Консульства в сопровождении родственника-гражданина Европейского Союза с документом, подтверждающим степень родства, приглашением от гражданина ЕС и билетом на самолет.\n" +
            "o\tВ Визовом центре действует официальный запрет на пользование мобильными электронными устройствами для сотрудников и посетителей: все устройства должны быть отключены на весь период пребывания в Визовом центре.\n" +
            "o\tПодать документы может лично заявитель, а в случае, если отпечатки пальцев сдавались ранее, за него это может сделать один из членов семьи, при предоставлении оригиналов и копий документов, подтверждающих родство (родители, совершеннолетние дети, супруг, супруга), либо иное лицо по нотариально заверенной доверенности (необходимо предоставить оригинал и 3 копии, а также копию паспорта доверенного лица — страницы с фотографией и пропиской в 3 экземплярах.\n" +
            "o\tЗаявку на получение визы в Визовый центр г. Москвы могут подавать граждане за исключением тех кто, зарегистрирован в Архангельской, Вологодской, Ленинградской, Мурманской, Новгородской, Псковской областях, а также Республике Карелия. Данным заявителям необходимо обращаться за получением итальянской визы в Визовый центр в Санкт-Петербурге.\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tИнформация о посольствах и визовых центрах:\n" +
            "•\tГенеральное Консульство Итальянской Республики в Москве\n" +
            "o\tАдрес:\t119180 Москва, Якиманская наб., д. 10\n" +
            "o\tТелефон: +7 (495) 796-96-92; +7 (495) 916-54-49; +7 (495) 916-54-51\n" +
            "o\tE-mail: consitaly.mosca@esteri.it\n" +
            "o\tОфициальный веб-сайт: http://www.consmosca.esteri.it/consolato_mosca/ru/\n" +
            "•\tГенеральное Консульство Итальянской Республики в Санкт-Петербурге\n" +
            "o\tАдрес: 190068 Санкт-Петербург, Театральная пл., 10\n" +
            "o\tТелефон: +7 (812) 312-32-17; +7 (812) 312-31-06; +7 (812) 718-80-95\n" +
            "o\tE-mail: segreteria.sanpietroburgo@esteri.it\n" +
            "o\tОфициальный веб-сайт: https://conssanpietroburgo.esteri.it/consolato_sanpietroburgo/ru/\n" +
            "\n" +
            "•\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tОфициальные визовые центры компании VMS\n" +
            "o\tФилиалы в юрисдикции Генерального Консульства Италии в Москве:\n" +
            "1)\tМосква - М. Толмачевский пер., д. 6, стр.1; +7(495)990-75-00\n" +
            "2)\tМосква - ул. Киевская, вл. 2; +7(495)990-75-00\n" +
            "3)\tМосква - ул. Мытная, д. 1, стр. 1; +7(495)249-07-77\n" +
            "4)\tБелгород - Гражданский пр-т, 47; +7(4722)424-962\n" +
            "5)\tВладивосток - ул. Светланская, 78б; +7(423)279-56-81\n" +
            "6)\tВоронеж - ул. Арсенальная, 3; +7(473)204-53-39\n" +
            "7)\tВолгоград - ул. Калинина, 13; +7(8442)55-01-06\n" +
            "8)\tЕкатеринбург - ул. Воеводина, 8; +7(343)318-3168\n" +
            "9)\tИркутск - ул. Свердлова, 36, оф 519, +7(3952)48-58-86\n" +
            "10)\tКазань - ул. Островского, 87; +7(843)567-11-02\n" +
            "11)\tКалининград - ул. Театральная, 35 оф 410; +7(4012)971-046\n" +
            "12)\tКраснодар - ул. Красных Партизан, 152, оф. 302; +7(861)203-40-80\n" +
            "13)\tКрасноярск - пр-т Мира, д. 91, ом. 60, офис 26; +7(391)257-35-57\n" +
            "14)\tЛипецк - пр-т Победы, 29; +7(4742)522-380\n" +
            "15)\tМинеральные Воды - Международный аэропорт Минеральные Воды; +7(87922)2-08-84\n" +
            "16)\tНижний Новгород - ул. Ульянова, 46; +7(831)215-22-15\n" +
            "17)\tНовосибирск - ул. Октябрьская, 42; +7(383)238-07-23\n" +
            "18)\tОмск - ул. Маяковского, 81; +7(3812)666-749\n" +
            "19)\tПермь - ул. Петропавловская, 41, оф 214; +7(342)259-79-87\n" +
            "20)\tРостов-на-Дону - ул. Социалистическая, 74; +7(863)285-00-14\n" +
            "21)\tСамара - ул. Льва Толстого, 29; +7(846)375-31-65\n" +
            "22)\tСтаврополь - ул. Доваторцев, 55А; +7(8652)50-14-00\n" +
            "23)\tТомск - пр-кт Ленина, 60/1; +7(3822)984-151\n" +
            "24)\tУльяновск - ул. Гончарова, 27; +7(8422)277-830\n" +
            "25)\tУфа - ул. Чернышевского, 82; +7(347)246-26-79\n" +
            "26)\tЧелябинск - ул. Советская, 25; +7(351)799-50-36\n" +
            "o\tФилиалы в юрисдикции Генерального Консульства Италии в Санкт-Петербурге\n" +
            "1)\tСанкт-Петербург - ул. Казанская, 1/25; +7(812)334-80-48\n" +
            "2)\tАрхангельск - наб. Северной Двины, 55 В, офис 605 ; +7(8182)69-85-33\n" +
            "3)\tПетрозаводск - ул. Свердлова, 18, оф. 405 ; +7(8142)77-50-73\n" +
            "4)\tПсков - ул. Льва Толстого, 1 офис 207 ; +7(8112)33-10-11\n" +
            "\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tНормативно-правовые акты\n" +
            "1)\t\"Соглашение между Российской Федерацией и Европейским сообществом об упрощении выдачи виз гражданам Российской Федерации и Европейского Союза\" (заключено в г. Сочи 25.05.2006) http://www.consultant.ru/document/cons_doc_LAW_126335/\n" +
            "2)\tРегламент N 810/2009 Европейского парламента и Совета Европейского Союза \"Устанавливающий Кодекс Сообщества о визах (Визовый кодекс)\" http://www.consultant.ru/cons/cgi/online.cgi?req=doc&base=INT&n=48381#08373044479928631\n" +
            "3)\t\"Согласованный список подтверждающих документов, предоставляемых заявителями для получения Шенгенской визы в Российской Федерации\" http://www.consultant.ru/document/cons_doc_LAW_140995/\n" +
            "4)\tRegolamento (UE) 2018/1806 del 14 novembre 2018 che ha abrogato e sostituito il Regolamento (CE) 539/2001 https://eur-lex.europa.eu/legal-content/IT/TXT/PDF/?uri=CELEX:32018R1806&from=IT\n" +
            "5)\tRegolamento (CE) n. 562/2006 del Parlamento Europeo e del Consiglio del 15 marzo 2006 https://www.esteri.it/mae/normative/normativa_consolare/visti/20130912_codice_frontiere_vers_consolidata_2013.pdf\n" +
            "6)\tRegolamento (CE) n. 539/2001 del Consiglio (e successive modifiche ed integrazioni) https://www.esteri.it/mae/normative/normativa_consolare/visti/regolamento_539_2001.pdf\n" +
            "7)\tDecisione del Consiglio dell’8 giugno 2004 che istituisce il sistema di informazione visti (VIS) https://www.esteri.it/mae/normative/normativa_consolare/visti/decisione_consiglio_8-6-2004.pdf\n" +
            "8)\tRegolamento (UE) n. 610/2013 del Parlamento Europeo e del Consiglio del 26 giugno 2013 che modifica il regolamento (CE) n. 562/2006 https://www.esteri.it/mae/normative/normativa_consolare/visti/regolamento_610-2013.pdf\n" +
            "9)\tDecreto Legislativo 25 luglio 1998, n. 286 https://www.esteri.it/mae/resource/doc/2016/12/decreto_legislativo_25_luglio_1998_n_286.pdf\n" +
            "10)\tD.P.R. 31 agosto 1999, n.394 https://www.esteri.it/mae/resource/doc/2016/12/decreto_del_presidente_della_repubblica_31_agosto_1999_n394.pdf\n" +
            "11)\tDirettiva del Ministero dell'Interno del 1.3.2000 https://www.esteri.it/mae/normative/normativa_consolare/visti/direttiva_mininterno_1-3-2000.pdf\n" +
            "12)\tDecreto Legislativo 6 febbraio 2007, n. 30 https://www.esteri.it/mae/normative/normativa_consolare/visti/direttiva_mininterno_1-3-2000.pdf\n" +
            "13)\tDecreto interministeriale n. 850 dell’11 maggio 2011 https://www.esteri.it/mae/resource/doc/2017/08/decreto_interministeriale_21_07_2018_nuovo_visto_per_investitori.pdf\n";
    String infofin = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tВизовый сбор (включая детей от 6 лет) – 35 €\n" +
            "Дети, 0 - 5 лет - 0 €\n" +
            "Срочная обработка заявления для граждан Российской Федерации если срок рассмотрения 3 рабочих дня или меньше - 70 €\n" +
            "Помимо визовых сборов с каждого заявителя дополнительно взимается сервисный сбор в размере 27.20 евро (включая НДС).\n" +
            "Визовый сбор взымается в рублях с соответствии с установленным курсом обмена валют.\n" +
            "В случае отрицательного ответа на заявление, оплаченные сборы не возвращается.\n" +
            "\n" +
            "Заявители, подающие документы на визу в Визовом центре, должны оплатить визовый сбор в кассе центра наличными или банковской.\n" +
            "Срок рассмотрения заявления на получение визы\n" +
            "Около 15 дней \n" +
            "Заявление на визу можно подавать не раньше чем за три месяца до начала планируемой поездки.\n" +
            "Срок рассмотрения заявления может быть увеличен, если в отношении заявления требуются дополнительные разъяснения, в том числе, проведение предварительной консультации с центральными органами других государств-членов Шенгенского соглашения.\n" +
            "Информация о Посольствах и Визовых центрах:\n" +
            "Посольство Финляндии в Москве: Кропоткинский переулок 15/17\n" +
            "Генеральное консульство Финляндии в Санкт-Петербурге: Преображенская пл., 4\n" +
            "Генеральное консульство в Петрозаводске: ул. Гоголя 6, 2-ой этаж\n" +
            "Генеральное консульство в Мурманске: ул. Карла Либкнехта, д. 13\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tВизовые центры:\n" +
            "•\tВладивосток: Океанский проспект д. 17 (6 этаж)\n" +
            "•\tЕкатеринбург: ул. Куйбышева д.44 (2 этаж, вход с улицы Белинского)\n" +
            "•\tИркутск: ул. Свердлова, дом 10 (2 этаж)\n" +
            "•\tКазань: ул. Парижской коммуны, 8  (2 этаж)\n" +
            "•\tКалининград: ул. 1812 года, дом 126\n" +
            "•\tКраснодар: ул. Академика Павлова д. 64\n" +
            "•\tКрасноярск: ул. Маерчака, д.16\n" +
            "•\tНижний Новгород: ул. Щербакова 15\n" +
            "•\tНовосибирск: Улица Челюскинцев, 15\n" +
            "•\tОмск: ул. Фрунзе, д.1, корп.4, офис 713\n" +
            "•\tПермь: ул. Чернышевского 28, (2 этаж)\n" +
            "•\tРостов-на-Дону: ул. Троллейбусная, д. 24/2в, корп. 1\n" +
            "•\tСамара: ул. Мичурина, д. 78, офис 2 (2этаж)\n" +
            "•\tУфа: Ул. Чернышевского , дом 82, 3 этаж, офис №301\n" +
            "Визовые мини-центры:\n" +
            "•\tВладимир: ул. Чапаева, д. 5\n" +
            "•\tТула: ул. Тургеневская, 7\n" +
            "•\tИваново: ул. Калинина, д. 9/21\n" +
            "•\tКалуга: ул. Промышленная, д. 36а\n" +
            "•\tВолгоград: ул. Маршала Чуйкова, д. 73\n" +
            "•\tСаранск: ул. Терешковой, 7а\n" +
            "•\tБелгород: ул. Костюкова, 36д\n" +
            "•\tРязань: ул. Урицкого, 54\n" +
            "•\tТомск: ул. Красноармейская, 20\n" +
            "•\tКурск: ул. Карла Маркса, д. 15\n" +
            "•\tТюмень: ул. Максима Горького, 74\n" +
            "•\tСургут: Проспект Мира, 55\n" +
            "•\tСочи: ул. Шоссейная, 8а\n" +
            "•\tЛипецк: ул. Литаврина, 6а\n" +
            "•\tИжевск: Карлутская набережная, 9\n" +
            "•\tУльяновск: ул. Карла Либкнехта, 24/5a, стр 1\n" +
            "\n" +
            "Запись на прием в Визовый центр Финляндии в Москве: http://visa.finland.eu/Russia/Moscow/Russian/Schedule_an_Appointment.html\n" +
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tНормативно-правовые акты" +
            "Согласованный список подтверждающих документов, предоставляемых заявителями для получения Шенгенской визы в Российской Федерации\n" +
            "http://www.consultant.ru/document/cons_doc_LAW_140995/ \n" +
            "\n" +
            "Регламент N 810/2009 Европейского парламента и Совета Европейского Союза\n" +
            "\"Устанавливающий Кодекс Сообщества о визах (Визовый кодекс)\" \n" +
            "(Принят в г. Брюсселе 13.07.2009)\n" +
            "http://www.consultant.ru/cons/cgi/online.cgi?req=doc&base=INT&n=48381#09362030842881759 \n" +
            "\n" +
            "Соглашение между Российской Федерацией и Европейским сообществом об упрощении выдачи виз гражданам Российской Федерации и Европейского Союза\n" +
            "(заключено в г. Сочи 25.05.2006)\n" +
            "http://www.consultant.ru/document/cons_doc_LAW_126335/ \n" +
            "\n" +
            "Aliens Act (301/2004, amendments up to 1152/2010 included)\n" +
            "Ссылка: https://www.finlex.fi/en/laki/kaannokset/2004/en20040301.pdf \n" +
            "\n" +
            "Рекомендации Международной организации гражданской авиации (ICAO) к фотографиям на документах, удостоверяющих личность\n";

    /**
     * коллекция русских букв с соответствующей английской буквой
     */
    private static final Map<String, String> letters = new HashMap<String, String>();

    static {
        letters.put("А", "A");
        letters.put("Б", "B");
        letters.put("В", "V");
        letters.put("Г", "G");
        letters.put("Д", "D");
        letters.put("Е", "E");
        letters.put("Ё", "E");
        letters.put("Ж", "1");
        letters.put("З", "Z");
        letters.put("И", "I");
        letters.put("Й", "Y");
        letters.put("К", "K");
        letters.put("Л", "L");
        letters.put("М", "M");
        letters.put("Н", "N");
        letters.put("О", "O");
        letters.put("П", "P");
        letters.put("Р", "R");
        letters.put("С", "S");
        letters.put("Т", "T");
        letters.put("У", "U");
        letters.put("Ф", "F");
        letters.put("Х", "H");
        letters.put("Ц", "C");
        letters.put("Ч", "2");
        letters.put("Ш", "3");
        letters.put("Щ", "4");
        //   letters.put("Ы", ")");
        letters.put("Э", "|");
        letters.put("Ю", "5");
        letters.put("Я", "6");
        letters.put("а", "a");
        letters.put("б", "b");
        letters.put("в", "v");
        letters.put("г", "g");
        letters.put("д", "d");
        letters.put("е", "e");
        letters.put("ё", "e");
        letters.put("ж", "7");
        letters.put("з", "z");
        letters.put("и", "i");
        letters.put("й", "y");
        letters.put("к", "k");
        letters.put("л", "l");
        letters.put("м", "m");
        letters.put("н", "n");
        letters.put("о", "o");
        letters.put("п", "p");
        letters.put("р", "r");
        letters.put("с", "s");
        letters.put("т", "t");
        letters.put("у", "u");
        letters.put("ф", "f");
        letters.put("х", "h");
        letters.put("ц", "c");
        letters.put("ч", "8");
        letters.put("ш", "9");
        letters.put("щ", "*");
        letters.put("ъ", "_");
        letters.put("ы", "@");
        letters.put("ь", "!");
        letters.put("э", "%");
        letters.put("ю", "^");
        letters.put("я", "0");
        letters.put("-", "-");
        letters.put(" ", " ");
    }
    /**
     * метод для перевода текста из русских букв в текст из английских букв
     * @param text строка, которую нужно перевести
     * @return переведенная строка
     */
    public static String toTranslit(String text) {
        StringBuilder sb = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); i++) {
            String l = text.substring(i, i + 1);
            if (letters.containsKey(l)) {
                sb.append(letters.get(l));
            } else {
                sb.append(l);
            }
        }
        return sb.toString();
    }
    /**
     * метод для создания кнопок, отправки запроса на сервер и вывода списка документов
     * @param savedInstanceState объект, который хранит данные о состоянии
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_visa_list);
        vBackButton = (Button) findViewById(R.id.vback_button);
        specialbtnto2str = findViewById(R.id.infoCountry);
        enterbtn3 = findViewById(R.id.login_button);
        personalRoombtn3 = findViewById(R.id.personal_info_button);
        mExitButton = findViewById(R.id.exit_button);
        Arrays.fill(visa, "");

        Intent intent = getIntent();
        mTargetCountry = intent.getStringExtra("target_country");
        mTranslitCountryName = toTranslit(mTargetCountry);
        mCountryNameField = (TextView) findViewById(R.id.country_name);
        mCountryNameField.setText(mTargetCountry);

        mExitButton.setOnClickListener(this);

        mToken = loadToken();
        mUserPublicData = loadUserPublicData();
        /**
         * если пользователь авторизирован, то кнопка "Вход" становится невидимой
         */
        if (mToken.isEmpty()) {
            enterbtn3.setVisibility(View.VISIBLE);
            mExitButton.setVisibility(View.INVISIBLE);
            /**
            * в противном случае кнопка "Выход" становится невидимой
            */
        } else {
            enterbtn3.setVisibility(View.INVISIBLE);
            mExitButton.setVisibility(View.VISIBLE);
        }

// закачка всех виз
        Log.i(Config.APP_TAG, "Вхожу в начало работы");
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        Log.i(Config.APP_TAG, "Тут");
        DataHolderApi jsonPlaceHolderApi = retrofit.create(DataHolderApi.class);
        Log.i(Config.APP_TAG, "Тут2");
        //mTargetCountry="Bulgary";
        call = jsonPlaceHolderApi.getPost(mTranslitCountryName);
        Log.i(Config.APP_TAG, "Тут3");
        int k = 0;
        call.enqueue(new Callback<List<Post>>() {
            /**
             * успешная отправка запроса
             * @param call возврат call объекта
             * @param response результат будет в объекте response
             */
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                Log.i(Config.APP_TAG, "Тут4");
                if (!response.isSuccessful()) {
                    Log.e(Config.APP_TAG, "onResponse isn't successful!");
                    return;
                }
                posts = response.body();
                for (Post post :
                        response.body()) {
                    Log.i(Config.APP_TAG, "" + post.Schengen);
                }

                int i = 0;
                MutableLiveData<String[]> liveData = new MutableLiveData<>();
                for (Post post : posts) {
                    if (mTargetCountry.equals(post.getCountry())) {
                        visa[i] = post.getTitle();
                        if (visa[i].equals("EMPTY")) {
                            visa[i] = "";
                        }
                        visa = sort(visa);
                        i++;
                    }
                }
                visaLiveData.postValue(visa);
            }
            /**
             * ошибка при отправке запроса
             * @param call возврат call объекта
             * @param t ошибка
             */
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.e(Config.APP_TAG, "" + t);
            }
        });
        /**
         * фильтр по выбору времени визы
         */
        Spinner spinnertimevisa = (Spinner) findViewById(R.id.spinnertimevisa);
        spinnertimevisa.setPrompt("Выбор времени");
        ArrayAdapter<String> adaptertime = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, timevisa2);
        adaptertime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnertimevisa.setAdapter(adaptertime);
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Получаем выбранный объект
                String item1 = (String) parent.getItemAtPosition(position);
                if (!item1.equals("Время: не выбрано")) {
                    Toast.makeText(VisaListActivity.this, "Вы выбрали время визы: " + item1, Toast.LENGTH_LONG).show();
                    int index = getArrayIndex(timevisa2, item1);
                    timevisachoose = timevisa[index];
                    changebtn1 = 1;

                    int i = 0;
                    for (Post post : posts) {
                        visa[i] = "";
                        if (mTargetCountry.equals(post.getCountry()) && timevisachoose.equals(post.getFilter_time()) && typevisachoose == null && purposevisachoose == null) {
                            visa[i] = post.getTitle();
                        }

                        if (mTargetCountry.equals(post.getCountry()) && purposevisachoose != null && typevisachoose != null && timevisachoose != null) {
                            if (purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase()) && timevisachoose.equals(post.getFilter_time())) {
                                if (post.getSchengen() == 1) {
                                    if (typevisachoose.equals("Шенгенская")) {
                                        visa[i] = post.getTitle();
                                    }
                                } else {
                                    if (typevisachoose.equals("Национальная")) {
                                        visa[i] = post.getTitle();
                                    }
                                }
                            }
                        }


                        if (mTargetCountry.equals(post.getCountry()) && typevisachoose == null && purposevisachoose != null) {
                            if (purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase()) && timevisachoose.equals(post.getFilter_time())) {
                                visa[i] = post.getTitle();
                            }
                        }

                        if (mTargetCountry.equals(post.getCountry()) && purposevisachoose == null && typevisachoose != null) {
                            if (timevisachoose.equals(post.getFilter_time())) {
                                if (post.getSchengen() == 1) {
                                    if (typevisachoose.equals("Шенгенская")) {
                                        visa[i] = post.getTitle();
                                    }
                                } else {
                                    if (typevisachoose.equals("Национальная")) {
                                        visa[i] = post.getTitle();
                                    }
                                }
                            }
                        }


                        i++;

                    }
                    visa = sort(visa);
                    visaLiveData.postValue(visa);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        spinnertimevisa.setOnItemSelectedListener(itemSelectedListener);

        /**
         * фильтр по выбору типа визы
         */
        Spinner spinnertypevisa = (Spinner) findViewById(R.id.spinnertypevisa);
        spinnertypevisa.setPrompt("Выбор типа визы");
        ArrayAdapter<String> adaptertype = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, typevisa);
        adaptertype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnertypevisa.setAdapter(adaptertype);
        AdapterView.OnItemSelectedListener itemSelectedListener2 = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Получаем выбранный объект
                String item2 = (String) parent.getItemAtPosition(position);
                if (!item2.equals("Тип визы: не выбрано")) {
                    Toast.makeText(VisaListActivity.this, "Вы выбрали тип визы: " + item2, Toast.LENGTH_LONG).show();
                    typevisachoose = item2;
                    changebtn2 = 1;

                    int i = 0;
                    for (Post post : posts) {
                        visa[i] = "";
                        if (mTargetCountry.equals(post.getCountry()) && timevisachoose == null && purposevisachoose == null) {
                            if (post.getSchengen() == 1) {
                                if (typevisachoose.equals("Шенгенская")) {
                                    visa[i] = post.getTitle();
                                }
                            } else {
                                if (typevisachoose.equals("Национальная")) {
                                    visa[i] = post.getTitle();
                                }
                            }
                        }


                        if (mTargetCountry.equals(post.getCountry()) && purposevisachoose != null && typevisachoose != null && timevisachoose != null) {
                            if (purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase()) && timevisachoose.equals(post.getFilter_time())) {
                                if (post.getSchengen() == 1) {
                                    if (typevisachoose.equals("Шенгенская")) {
                                        visa[i] = post.getTitle();
                                    }
                                } else {
                                    if (typevisachoose.equals("Национальная")) {
                                        visa[i] = post.getTitle();
                                    }
                                }
                            }
                        }

                        if (mTargetCountry.equals(post.getCountry()) && timevisachoose == null && purposevisachoose != null) {
                            if (purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase())) {
                                if (post.getSchengen() == 1) {
                                    if (typevisachoose.equals("Шенгенская")) {
                                        visa[i] = post.getTitle();
                                    }
                                } else {
                                    if (typevisachoose.equals("Национальная")) {
                                        visa[i] = post.getTitle();
                                    }
                                }
                            }
                        }


                        if (mTargetCountry.equals(post.getCountry()) && purposevisachoose == null && timevisachoose != null) {
                            if (timevisachoose.equals(post.getFilter_time())) {
                                if (post.getSchengen() == 1) {
                                    if (typevisachoose.equals("Шенгенская")) {
                                        visa[i] = post.getTitle();
                                    }
                                } else {
                                    if (typevisachoose.equals("Национальная")) {
                                        visa[i] = post.getTitle();
                                    }
                                }
                            }
                        }
                        i++;
                    }
                    visa = sort(visa);
                    visaLiveData.postValue(visa);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        spinnertypevisa.setOnItemSelectedListener(itemSelectedListener2);

        /**
         * фильтр по выбору цели поездки
         */
        Spinner spinnerpurposevisa = (Spinner) findViewById(R.id.spinnerpurposevisa);
        spinnerpurposevisa.setPrompt("Выбор цели визы");
        ArrayAdapter<String> adapterpurpose = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, purposevisa);
        adapterpurpose.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerpurposevisa.setAdapter(adapterpurpose);
        AdapterView.OnItemSelectedListener itemSelectedListener3 = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Получаем выбранный объект
                String item3 = (String) parent.getItemAtPosition(position);
                if (!item3.equals("Цель визы: не выбрано")) {
                    Toast.makeText(VisaListActivity.this, "Вы выбрали цель визы: " + item3, Toast.LENGTH_LONG).show();
                    purposevisachoose = item3;
                    changebtn3 = 1;

                    int i = 0;
                    for (Post post : posts) {
                        visa[i] = "";
                        if (mTargetCountry.equals(post.getCountry()) && purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase()) && timevisachoose == null && typevisachoose == null) {
                            visa[i] = post.getTitle();
                        }

                        if (mTargetCountry.equals(post.getCountry()) && purposevisachoose != null && typevisachoose != null && timevisachoose != null) {
                            if (purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase()) && timevisachoose.equals(post.getFilter_time())) {
                                if (post.getSchengen() == 1) {
                                    if (typevisachoose.equals("Шенгенская")) {
                                        visa[i] = post.getTitle();
                                    }
                                } else {
                                    if (typevisachoose.equals("Национальная")) {
                                        visa[i] = post.getTitle();
                                    }
                                }
                            }
                        }

                        if (mTargetCountry.equals(post.getCountry()) && timevisachoose == null && typevisachoose != null) {
                            if (purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase())) {
                                if (post.getSchengen() == 1) {
                                    if (typevisachoose.equals("Шенгенская")) {
                                        visa[i] = post.getTitle();
                                    }
                                } else {
                                    if (typevisachoose.equals("Национальная")) {
                                        visa[i] = post.getTitle();
                                    }
                                }
                            }
                        }

                        if (mTargetCountry.equals(post.getCountry()) && typevisachoose == null && timevisachoose != null) {
                            if (purposevisachoose.toLowerCase().equals(post.getFilter_purpose().toLowerCase()) && timevisachoose.equals(post.getFilter_time())) {
                                visa[i] = post.getTitle();
                            }
                        }


                        i++;


                    }
                    visa = sort(visa);
                    visaLiveData.postValue(visa);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        spinnerpurposevisa.setOnItemSelectedListener(itemSelectedListener3);


        //формирование списка виз

        ListView list = (ListView) findViewById(R.id.listVisa);

        final ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, visa);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {   // по позиции получаем выбранный элемент
                String selectedItem = visa[position];
                Log.i(Config.APP_TAG, "Тут 5");
                if (!selectedItem.equals("Не выбрано")) {
                    mVisaId = position;
                    mVisaName = selectedItem;
                    Intent intent = new Intent();
                    intent.putExtra("visa_id", position);
                    intent.putExtra("visa_name", selectedItem);
                    intent.putExtra("target_country", mTargetCountry);
                    intent.setClass(VisaListActivity.this, DocumentsListActivity.class);
                    startActivity(intent);
                }
            }
        });
        /**
         * изменение данных в интерфейсе с помощью LiveData
         */
        visaLiveData.observe(this, new Observer<String[]>() {
            @Override
            public void onChanged(String[] strings) {
                adapter.notifyDataSetChanged();
            }
        });

        enterbtn3.setOnClickListener(this);
        personalRoombtn3.setOnClickListener(this);
        specialbtnto2str.setOnClickListener(this);


        vBackButton.setOnClickListener(this);
    }
    /**
     * активность получает передний план
     */
    @Override
    protected void onResume() {
        super.onResume();
        mToken = loadToken();
        if (!mToken.isEmpty()) {
            mUserPublicData = loadUserPublicData();
            enterbtn3.setVisibility(View.INVISIBLE);
            mExitButton.setVisibility(View.VISIBLE);
        }
    }
    /**
     * метод обработки нажатий на кнопки
     * @param v нажатая кнопка
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * обработка нажатия кнопки "Назад"
             */
            case R.id.vback_button:
                finish();
                break;
            /**
             * обработка нажатия кнопки "Вход"
             */
            case R.id.login_button:
                if (activationOfAccount == 0) {
                    Intent intent = new Intent(this, SignInActivity.class);
                    intent.putExtra("visa_id", mVisaId);
                    intent.putExtra("visa_name", mVisaName);
                    intent.putExtra("target_country", mTargetCountry);


                    startActivity(intent);
                } else {
                    Toast.makeText(VisaListActivity.this, "Вы уже вошли", Toast.LENGTH_LONG).show();
                    //тут вывод сообщения: необходимо зарегаться
                    ;
                }
                break;
            /**
             * обработка нажатия кнопки "Выход"
             */
            case R.id.exit_button:
                logout();
                break;
            default:
                break;
            /**
             * обработка нажатия кнопки "Личный кабинет"
             */
            case R.id.personal_info_button:
                Intent intent = new Intent(this, AccountActivity.class);
                intent.putExtra("visa_id", mVisaId);
                intent.putExtra("visa_name", mVisaName);
                intent.putExtra("target_country", mTargetCountry);
                startActivity(intent);
                Toast.makeText(VisaListActivity.this, "Необходимо войти", Toast.LENGTH_LONG).show();
                break;
            /**
             * обработка нажатия кнопки "Инфо"
             */
            case R.id.infoCountry:
                if (!mTargetCountry.equals("Финляндия") && !mTargetCountry.equals("Италия") && !mTargetCountry.equals("Болгария")) {
                    Toast.makeText(VisaListActivity.this, "Дополнительная информация о данной стране на данный момент отсутствует", Toast.LENGTH_LONG).show();
                    break;
                }

                final Dialog dialog = new Dialog(VisaListActivity.this);
                dialog.setContentView(R.layout.dialog_layout);
                TextView text = (TextView) dialog.findViewById(R.id.TextView01);

                if (mTargetCountry.equals("Болгария")) {
                    text.setText(infobulgary);
                }
                if (mTargetCountry.equals("Италия")) {
                    text.setText(infoitaly);
                }
                if (mTargetCountry.equals("Финляндия")) {
                    text.setText(infofin);
                }

                Button button = dialog.findViewById(R.id.button2);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.setCancelable(false);
                dialog.setTitle("Информация о визовых центрах и консульствах для выбранной страны");
                dialog.show();

                break;

        }
    }
    /**
     * метод сортировки дкументов
     * @param example строка названия документа
     * @return возвращаемое значение строки
     */
    public String[] sort(String[] example) {
        for (int i = 0; i < example.length; i++) {
            for (int j = 0; j < example.length; j++) {
                if (example[j].isEmpty()) {
                    for (int k = j + 1; k < example.length; k++) {
                        example[k - 1] = example[k];
                    }
                    example[example.length - 1 - j] = "";
                    break;
                }
            }
        }

        return example;
    }

    public int getArrayIndex(String[] arr, String value) {
        int k = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                k = i;
                break;
            }
        }
        return k;
    }
    /**
     * метод загрузки токена
     * @return возвращает токен
     */
    private String loadToken() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        String token = pref.getString("token",
                "");
        return token;
    }
    /**
     * метод загрузки информации о пользователе
     * @return возвращает объект data
     */
    private UserPublicData loadUserPublicData() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        UserPublicData data = new UserPublicData();
        data.mFirstName = pref.getString("first_name",
                "");
        data.mMiddleName = pref.getString("middle_name",
                "");
        data.mLastName = pref.getString("last_name",
                "");
        return data;
    }
    /**
     * метод выхода из системы
     */
    private void logout() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        Toast.makeText(
                this,
                "До встречи " + mUserPublicData.mFirstName + " !",
                Toast.LENGTH_SHORT)
                .show();
        pref.edit().clear().apply();
        mToken = "";
        enterbtn3.setVisibility(View.VISIBLE);
        mExitButton.setVisibility(View.INVISIBLE);
    }
}

