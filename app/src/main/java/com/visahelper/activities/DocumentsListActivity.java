package com.visahelper.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visahelper.config.Config;
import com.visahelper.R;
import com.visahelper.data.DataHolderApi;
import com.visahelper.data.Post2;
import com.visahelper.data.UserPublicData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * класс для формирования и вывода списка документов, необходимых для оформления выбранной визы
 * @author Nargiz
 * @version 1.0
 */
public class DocumentsListActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * кнопки для перехода на другие страницы
     */
    private Button spetialbtnto3str, mExitButton, dBackButton, btnenter3, btnpersonalRoom3, moveto3or5strbtn, btntomain, infodocs;
    /**
     * переменные для хранения информации об авторизованности пользователя в системе
     */
    int activationOfAccount, activationOfAccount2 = 0;
    /**
     * текстовое поле для вывода названия визы
     */
    TextView mVisaNameField;
    /**
     * переменная, хранящая id визы
     */
    private int mVisaId;
    /**
     * переменная, хранящая страну
     */
    private String mTargetCountry;
    /**
     * переменная, хранящая название визы
     */
    private String mVisaName;
    /**
     *переменная, хранящая токен пользователя
     */
    private String mToken;
    /**
     * переменная, хранящая данные пользователя
     */
    private UserPublicData mUserPublicData;
    /**
     * переменная хранения выводимых документов
     */
    String[] documents = new String[24];
    /**
     * хранилище данных MutableLiveData
     */
    MutableLiveData<String[]> documentsLiveData = new MutableLiveData<>();
    /**
     * call2 - переменная, которая хранит возвращаемое значение типа Call
     */
    Call<List<Post2>> call2;
    List<Post2> posts2;
    /**
     * коллекция русских букв с соответствующей английской буквой
     */
    private static final Map<String, String> letters = new HashMap<String, String>();
    static {
        letters.put("А", "A");
        letters.put("Б", "B");
        letters.put("В", "V");
        letters.put("Г", "G");
        letters.put("Д", "D");
        letters.put("Е", "E");
        letters.put("Ё", "E");
        letters.put("Ж", "1");
        letters.put("З", "Z");
        letters.put("И", "I");
        letters.put("Й", "Y");
        letters.put("К", "K");
        letters.put("Л", "L");
        letters.put("М", "M");
        letters.put("Н", "N");
        letters.put("О", "O");
        letters.put("П", "P");
        letters.put("Р", "R");
        letters.put("С", "S");
        letters.put("Т", "T");
        letters.put("У", "U");
        letters.put("Ф", "F");
        letters.put("Х", "H");
        letters.put("Ц", "C");
        letters.put("Ч", "2");
        letters.put("Ш", "3");
        letters.put("Щ", "4");
        //   letters.put("Ы", ")");
        letters.put("Э", "E");
        letters.put("Ю", "5");
        letters.put("Я", "6");
        letters.put("а", "a");
        letters.put("б", "b");
        letters.put("в", "v");
        letters.put("г", "g");
        letters.put("д", "d");
        letters.put("е", "e");
        letters.put("ё", "e");
        letters.put("ж", "7");
        letters.put("з", "z");
        letters.put("и", "i");
        letters.put("й", "y");
        letters.put("к", "k");
        letters.put("л", "l");
        letters.put("м", "m");
        letters.put("н", "n");
        letters.put("о", "o");
        letters.put("п", "p");
        letters.put("р", "r");
        letters.put("с", "s");
        letters.put("т", "t");
        letters.put("у", "u");
        letters.put("ф", "f");
        letters.put("х", "h");
        letters.put("ц", "c");
        letters.put("ч", "8");
        letters.put("ш", "9");
        letters.put("щ", "*");
        letters.put("ъ", "_");
        letters.put("ы", "@");
        letters.put("ь", "!");
        letters.put("э", "%");
        letters.put("ю", "^");
        letters.put("я", "0");
        letters.put("-", "-");
        letters.put(" ", " ");
    }

    /**
     * метод для перевода текста из русских букв в текст из английских букв
     * @param text строка, которую нужно перевести
     * @return переведенная строка
     */
    public static String toTranslit(String text) {
        StringBuilder sb = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); i++) {
            String l = text.substring(i, i + 1);
            if (letters.containsKey(l)) {
                sb.append(letters.get(l));
            } else {
                sb.append(l);
            }
        }
        return sb.toString();
    }

    /**
     * метод для создания кнопок, отправки запроса на сервер и вывода списка документов
     * @param savedInstanceState объект, который хранит данные о состоянии
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_docs_list);
        dBackButton = (Button) findViewById(R.id.dback_button);
        mExitButton = (Button) findViewById(R.id.exit_button);
        Intent intent = getIntent();
        mTargetCountry = intent.getStringExtra("target_country");
        mVisaId = intent.getIntExtra("visa_id", 0);
        mVisaName = intent.getStringExtra("visa_name");

        if (activationOfAccount2 == 1) {
            activationOfAccount = activationOfAccount2;
        }

        mVisaNameField = (TextView) findViewById(R.id.country_name);
        mVisaNameField.setText(String.format(mVisaName));

        for (int i = 0; i < 24; i++) {
            documents[i] = "";
        }

        String vi = toTranslit(mVisaName).toLowerCase();
        String coun = toTranslit(mTargetCountry);

        Log.i(Config.APP_TAG, "Вхожу в начало работы");
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        Log.i(Config.APP_TAG, "Тут");
        DataHolderApi jsonPlaceHolderApi2 = retrofit.create(DataHolderApi.class);
        call2 = jsonPlaceHolderApi2.getPost2(coun, vi);
        Log.i(Config.APP_TAG, "кодировочка визы: " + vi);
        call2.enqueue(new Callback<List<Post2>>() {
            @Override
            /**
             * успешная отправка запроса
             * @param call2 возврат call объекта
             * @param response результат будет в объекте response
             */
            public void onResponse(Call<List<Post2>> call2, Response<List<Post2>> response) {
                if (!response.isSuccessful()) {
                    Log.e(Config.APP_TAG, "Ошибка: плохой Response");
                    mVisaNameField.setText("Code: " + response.code());
                    return;
                }
                posts2 = response.body();
                int i = 0;
                Log.i(Config.APP_TAG, "Стою перед циклом");
                /**
                 * получение документов из БД
                 */
                for (Post2 post2 : posts2) {
                    Log.i(Config.APP_TAG, "Пытаюсь работать");
                    //тут формирование списка документов
                    documents[i] = post2.getDocument();
                    if (documents[i].equals("EMPTY")) {
                        documents[i] = "";
                    }
                    i++;
                    Log.i(Config.APP_TAG, "Все работает");
                }
                documents = sort(documents);
                documentsLiveData.postValue(documents);

                Log.i(Config.APP_TAG, "Количество документов:" + i);
            }
            /**
             * ошибка при отправке запроса
             * @param call возврат call объекта
             * @param t ошибка
             */
            @Override
            public void onFailure(Call<List<Post2>> call, Throwable t) {
                Log.e(Config.APP_TAG, "" + t);
                //textView.setText(t.getMessage());
            }
        });


        ListView list = (ListView) findViewById(R.id.listdocs);
        //  ListView infoaboutdocslist=(ListView) findViewById(R.id.infoaboutdocslist);

        final ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_multiple_choice, documents);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /**
             * метод обработки пометки "галочкой" докуметов
             * @param position позиция документа
             * @param id id документа
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {   // имеем массив chosen выбранных элементов
                SparseBooleanArray chosen = ((ListView) parent).getCheckedItemPositions();
                for (int i = 0; i < chosen.size(); i++) {
                    //chosedocsbycustomer[i]=chosen.keyAt(i);

                    //тут отправка массива в БД

                    /*
                    if (chosen.valueAt(i)){
                        sluzhebnayatext.append(documents[chosen.keyAt(i)] + " ");
                    }*/

                }
            }
        });
        /**
         * изменение данных в интерфейсе с помощью LiveData
         */
        documentsLiveData.observe(this, new Observer<String[]>() {
            @Override
            public void onChanged(String[] strings) {
                adapter.notifyDataSetChanged();
            }
        });


        spetialbtnto3str = (Button) findViewById(R.id.infovisa);
        btnenter3 = (Button) findViewById(R.id.login_button);
        btnpersonalRoom3 = (Button) findViewById(R.id.personal_info_button);
        moveto3or5strbtn = (Button) findViewById(R.id.moveto3or5strbtn);
        btntomain = (Button) findViewById(R.id.btntomain);
        infodocs = (Button) findViewById(R.id.infodocs);

        spetialbtnto3str.setOnClickListener(this);
        btnenter3.setOnClickListener(this);
        btnpersonalRoom3.setOnClickListener(this);
        moveto3or5strbtn.setOnClickListener(this);
        btntomain.setOnClickListener(this);
        infodocs.setOnClickListener(this);
        dBackButton.setOnClickListener(this);

        mExitButton.setOnClickListener(this);
        mToken = loadToken();
        mUserPublicData = loadUserPublicData();
        /**
         * если пользователь авторизирован, то кнопка "Вход" становится невидимой
         */
        if (mToken.isEmpty()) {
            btnenter3.setVisibility(View.VISIBLE);
            mExitButton.setVisibility(View.INVISIBLE);
            /**
             * в противном случае кнопка "Выход" становится невидимой
             */
        } else {
            btnenter3.setVisibility(View.INVISIBLE);
            mExitButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * активность получает передний план
     */
    @Override
    protected void onResume() {
        super.onResume();
        mToken = loadToken();
        if (!mToken.isEmpty()) {
            mUserPublicData = loadUserPublicData();
            btnenter3.setVisibility(View.INVISIBLE);
            mExitButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * метод обработки нажатий на кнопки
     * @param v нажатая кнопка
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * обработка нажатия кнопки "Назад"
             */
            case R.id.dback_button:
                finish();
                break;
            /**
             * обработка нажатия кнопки "Выход"
             */
            case R.id.exit_button:
                logout();
                break;
            default:
                break;
            /**
             * обработка нажатия кнопки "Информация о визе"
             */
            case R.id.infovisa:
                Intent intent = new Intent(this, VisaInfoActivity.class);
                intent.putExtra("target_country", mTargetCountry);
                intent.putExtra("visa_id", mVisaId);
                intent.putExtra("visa_name", mVisaName);

                startActivity(intent);
                break;
            /**
             * обработка нажатия кнопки "Вход"
             */
            case R.id.login_button:
                if (activationOfAccount == 0) {
                    Intent intent1 = new Intent(this, SignInActivity.class);
                    intent1.putExtra("target_country", mTargetCountry);
                    intent1.putExtra("visa_id", mVisaId);
                    intent1.putExtra("visa_name", mVisaName);
                    startActivity(intent1);
                } else {
                    Toast.makeText(DocumentsListActivity.this, "Вы уже вошли", Toast.LENGTH_LONG).show();
                }
                break;
            /**
             * обработка нажатия кнопки "Личный кабинет"
             */
            case R.id.personal_info_button:

                Intent intent2 = new Intent(this, AccountActivity.class);
                intent2.putExtra("target_country", mTargetCountry);
                intent2.putExtra("visa_id", 0);
                intent2.putExtra("visa_name", "");
                startActivity(intent2);
                break;
            /**
             * обработка нажатия кнопки "Оформление документов"
             */
            case R.id.moveto3or5strbtn:
                mToken = loadToken();
                if (mToken.isEmpty()) {
                    Intent intent8 = new Intent(this, SignInActivity.class);
                    startActivity(intent8);
                } else {
                    Intent intent9 = new Intent(this, AccountActivity.class);
                    intent9.putExtra("target_country", mTargetCountry);
                    intent9.putExtra("visa_id", mVisaId);
                    intent9.putExtra("visa_name", mVisaName);
                    startActivity(intent9);
                }
                break;
            /**
             * обработка нажатия кнопки "На главную"
             */
            case R.id.btntomain:
                Intent intent1 = new Intent(this, MainActivity.class);
                startActivity(intent1);
                break;
            /**
             * обработка нажатия кнопки "Информация о документах"
             */
            case R.id.infodocs:
                 {
                    Intent intent80 = new Intent(this, DocumentInfoActivity.class);
                    intent80.putExtra("target_country", mTargetCountry);
                    intent80.putExtra("visa_id", mVisaId);
                    intent80.putExtra("visa_name", mVisaName);
                    startActivity(intent80);
                }


                break;

        }
    }

    /**
     * метод сортировки дкументов
     * @param example строка названия документа
     * @return возвращаемое значение строки
     */
    public String[] sort(String[] example) {
        for (int i = 0; i < example.length; i++) {
            for (int j = 0; j < example.length; j++) {
                if (example[j] == "") {
                    for (int k = j + 1; k < example.length; k++) {
                        example[k - 1] = example[k];
                    }
                    example[example.length - 1 - j] = "";
                    break;
                }
            }
        }

        return example;
    }

    /**
     * метод загрузки токена
     * @return возвращает токен
     */
    private String loadToken() {
        SharedPreferences pref = getSharedPreferences("user_data", MODE_PRIVATE);
        String token = pref.getString("token", "");
        return token;
    }

    /**
     * метод загрузки информации о пользователе
     * @return возвращает объект data
     */
    private UserPublicData loadUserPublicData() {
        SharedPreferences pref = getSharedPreferences("user_data", MODE_PRIVATE);
        UserPublicData data = new UserPublicData();
        data.mFirstName = pref.getString("first_name", "");
        data.mMiddleName = pref.getString("middle_name", "");
        data.mLastName = pref.getString("last_name", "");
        return data;
    }

    /**
     * метод выхода из системы
     */
    private void logout() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        Toast.makeText(
                this,
                "До встречи, " + mUserPublicData.mFirstName + " !",
                Toast.LENGTH_SHORT)
                .show();
        pref.edit().clear().apply();
        mToken = "";
        btnenter3.setVisibility(View.VISIBLE);
        mExitButton.setVisibility(View.INVISIBLE);
    }

}
