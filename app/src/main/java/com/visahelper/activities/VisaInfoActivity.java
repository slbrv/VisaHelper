package com.visahelper.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visahelper.R;
import com.visahelper.config.Config;
import com.visahelper.data.DataHolderApi;
import com.visahelper.data.Post5;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class VisaInfoActivity extends AppCompatActivity implements View.OnClickListener{
    //Button backtostr2;
    int activationOfAccount;
    String Infs = null;
    TextView vVisaNameField, infoaboutvisa;
    String namevisa = null, namecountry = null, informationaboutvisatext = "фхфхфхфхх Дополнительная информация о данной Визе отсутствует";
    private Button viBackButton;
    private int vVisaId;
    private String vTargetCountry;
    private String vVisaName;

    MutableLiveData<String[]> InfsLiveData = new MutableLiveData<>();

    Call<List<Post5>> call5;
    List<Post5> posts5;

    private static final String TAG = "MyApp";

    private String mToken;

    private static final Map<String, String> letters = new HashMap<String, String>();

    static {
        letters.put("А", "A");
        letters.put("Б", "B");
        letters.put("В", "V");
        letters.put("Г", "G");
        letters.put("Д", "D");
        letters.put("Е", "E");
        letters.put("Ё", "E");
        letters.put("Ж", "1");
        letters.put("З", "Z");
        letters.put("И", "I");
        letters.put("Й", "Y");
        letters.put("К", "K");
        letters.put("Л", "L");
        letters.put("М", "M");
        letters.put("Н", "N");
        letters.put("О", "O");
        letters.put("П", "P");
        letters.put("Р", "R");
        letters.put("С", "S");
        letters.put("Т", "T");
        letters.put("У", "U");
        letters.put("Ф", "F");
        letters.put("Х", "H");
        letters.put("Ц", "C");
        letters.put("Ч", "2");
        letters.put("Ш", "3");
        letters.put("Щ", "4");
// letters.put("Ы", ")");
        letters.put("Э", "E");
        letters.put("Ю", "5");
        letters.put("Я", "6");
        letters.put("а", "a");
        letters.put("б", "b");
        letters.put("в", "v");
        letters.put("г", "g");
        letters.put("д", "d");
        letters.put("е", "e");
        letters.put("ё", "e");
        letters.put("ж", "7");
        letters.put("з", "z");
        letters.put("и", "i");
        letters.put("й", "y");
        letters.put("к", "k");
        letters.put("л", "l");
        letters.put("м", "m");
        letters.put("н", "n");
        letters.put("о", "o");
        letters.put("п", "p");
        letters.put("р", "r");
        letters.put("с", "s");
        letters.put("т", "t");
        letters.put("у", "u");
        letters.put("ф", "f");
        letters.put("х", "h");
        letters.put("ц", "c");
        letters.put("ч", "8");
        letters.put("ш", "9");
        letters.put("щ", "*");
        letters.put("ъ", "_");
        letters.put("ы", "@");
        letters.put("ь", "!");
        letters.put("э", "%");
        letters.put("ю", "^");
        letters.put("я", "0");
        letters.put("-", "-");
        letters.put(" ", " ");
    }

    String vi, coun, idperson = null;

    public static String toTranslit(String text) {
        StringBuilder sb = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); i++) {
            String l = text.substring(i, i + 1);
            if (letters.containsKey(l)) {
                sb.append(letters.get(l));
            } else {
                sb.append(l);
            }
        }
        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infovisa);
        viBackButton = (Button) findViewById(R.id.viback_button);
        Intent intent0 = getIntent();

        /* namecountry = intent0.getStringExtra("countryname1");
        namevisa = intent0.getStringExtra("visaname1");
        activationOfAccount = intent0.getIntExtra("activationOfAccount1", activationOfAccount);

        visaname = (TextView) findViewById(R.id.visaname);
        visaname.setText(String.format(namevisa));
        */

        vTargetCountry = intent0.getStringExtra("target_country");
        vVisaId = intent0.getIntExtra("visa_id", 0);
        vVisaName = intent0.getStringExtra("visa_name");

        //vVisaNameField = (TextView) findViewById(R.id.visaname);
       // vVisaNameField.setText(String.format(vVisaName));

        vi = toTranslit(vVisaName).toLowerCase();
        coun = toTranslit(vTargetCountry);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        Log.i(Config.APP_TAG, "Тут infovisa");
        DataHolderApi jsonPlaceHolderApi5 = retrofit.create(DataHolderApi.class);
        call5 = jsonPlaceHolderApi5.getPost5(coun, vi);
        Log.i(Config.APP_TAG, "кодировочка визы: " + vi);
        call5.enqueue(new Callback<List<Post5>>() {
            @Override
            public void onResponse(Call<List<Post5>> call5, Response<List<Post5>> response) {
                if (!response.isSuccessful()) {
                    Log.e(Config.APP_TAG, "Ошибка: плохой Response");
                    vVisaNameField.setText("Code: " + response.code());
                    return;
                }

                posts5 = response.body();
                int i = 0;
                Log.i(Config.APP_TAG, "Стою перед циклом infovisa");
                for (Post5 post5 : posts5) {
                    Log.i(Config.APP_TAG, "Пытаюсь работать infovisa");
                    Infs = post5.getInf();
                    if (Infs.equals("EMPTY")) {
                        Infs = "";
                    }
                    i++;
                    Log.i(Config.APP_TAG, "Все работает infovisa");
                }

                Log.i(Config.APP_TAG, "Количество документов: infovisa " + i + Infs);

                Log.i(Config.APP_TAG, "222 Количество документов: infovisa " + Infs);
                infoaboutvisa = (TextView) findViewById(R.id.infoaboutvisa);
                infoaboutvisa.setText(infoaboutvisa.getText().toString() + Infs);
            }

            @Override
            public void onFailure(Call<List<Post5>> call5, Throwable t) {
                Log.e(TAG, "" + t);
//textView.setText(t.getMessage());
            }
        });
        viBackButton.setOnClickListener((View.OnClickListener) this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viback_button:
                finish();
                break;
        }
    }

    public String[] sort(String[] example) {
        for (int i = 0; i < example.length; i++) {
            for (int j = 0; j < example.length; j++) {
                if (example[j] == "") {
                    for (int k = j + 1; k < example.length; k++) {
                        example[k - 1] = example[k];
                    }
                    example[example.length - 1 - j] = "";
                    break;
                }
            }
        }

        return example;
    }

}