package com.visahelper.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.visahelper.R;
import com.visahelper.config.Config;
import com.visahelper.data.UserPublicData;

/**
 * класс главной страницы
 * @author Nargiz
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * переменная кнопки поиска
     */
    private Button mSearchButton;
    /**
     *
     */
    private Button mEnterButton;
    /**
     * переменная кнопки перехода в личный кабинет
     */
    private Button mPersonalInfoButton;
    /**
     * переменная кнопки справка
     */
    private Button mCertificateButton;
    /**
     * переменная кнопки перехода в полезные статьи
     */
    private Button mAdditionalInfoButton;
    /**
     * переменная кнопки перехода в интересные статьи
     */
    private Button mInterestsButton;
    /**
     * переменная кнопки выхода
     */
    private Button mExitButton;
    /**
     * переменная, хранящая токен
     */
    private String mToken;
    /**
     * переменная, хранящая личные данные пользователя
     */
    private UserPublicData mUserPublicData;
    /**
     * переменная, хранящая страну
     */
    private String mTargetCountry;
    String kindarticle;
    /**
     * переменная, хранящая список стран
     */
    String[] mCountryList = {
            "Страна: не выбрано",
            "Австрия",
            "Албания",
            "Андорра",
            "Бельгия",
            "Болгария",
            "Босния и Герцеговина",
            "Великобритания",
            "Венгрия",
            "Германия",
            "Греция",
            "Дания",
            "Ирландия",
            "Исландия",
            "Испания",
            "Италия",
            "Латвия",
            "Литва",
            "Лихтенштейн",
            "Люксембург",
            "Мальта",
            "Монако",
            "Нидерланды",
            "Норвегия",
            "Польша",
            "Португалия",
            "Румыния",
            "Сан-Марино",
            "Северная Македония",
            "Сербия",
            "Словакия",
            "Словения",
            "Финляндия",
            "Франция",
            "Хорватия",
            "Черногория",
            "Чехия",
            "Швейцария",
            "Швеция",
            "Эстония"};

    /**
     * метод, создающий элементы страницы и обрабатывающий выбор страны
     * @param savedInstanceState объект, который хранит данные о состоянии
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // список
        Spinner spinner = findViewById(R.id.alfavitespinner);
        spinner.setPrompt("Выбор страны");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                mCountryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            /**
             * метод, обрабатывающий выбор страны из списка
             * @param parent родительский список
             * @param position позиция
             * @param id id страны
             */
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Получаем выбранный объект
                String item = (String) parent.getItemAtPosition(position);
                if (!item.equals("Страна: не выбрано")) {
                    Toast.makeText(MainActivity.this,
                            "Вы выбрали: " + item,
                            Toast.LENGTH_LONG).show();
                    Intent intent = new Intent();
                    intent.putExtra("target_country", item);
                    intent.setClass(MainActivity.this,
                            VisaListActivity.class);
                    startActivity(intent);
                    mTargetCountry = item;
                }
            }

            /**
             * метод "если ничего не выбрано"
             * @param parent родительский список
             */
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };

        spinner.setOnItemSelectedListener(itemSelectedListener);

        mSearchButton = findViewById(R.id.search_button);
        mEnterButton = findViewById(R.id.login_button);
        mPersonalInfoButton = findViewById(R.id.personal_info_button);
        mCertificateButton = findViewById(R.id.certificate_button);
        mAdditionalInfoButton = findViewById(R.id.additional_info_button);
        mInterestsButton = findViewById(R.id.interests_button);
        mExitButton = findViewById(R.id.exit_button);

        mSearchButton.setOnClickListener(this);
        mEnterButton.setOnClickListener(this);
        mPersonalInfoButton.setOnClickListener(this);
        mCertificateButton.setOnClickListener(this);
        mAdditionalInfoButton.setOnClickListener(this);
        mInterestsButton.setOnClickListener(this);
        mExitButton.setOnClickListener(this);

        mToken = loadToken();
        mUserPublicData = loadUserPublicData();

        Log.i(Config.APP_TAG,
                "Token load: " + mToken);
        /**
         * если пользователь авторизирован, то кнопка "Вход" становится невидимой
         */
        if (mToken.isEmpty()) {
            mEnterButton.setVisibility(View.VISIBLE);
            mExitButton.setVisibility(View.INVISIBLE);
         /**
         * в противном случае кнопка "Выход" становится невидимой
         */
        } else {
            mEnterButton.setVisibility(View.INVISIBLE);
            mExitButton.setVisibility(View.VISIBLE);
        }
    }
    /**
     * активность получает передний план
     */
    @Override
    protected void onResume() {
        super.onResume();
        mToken = loadToken();
        if (!mToken.isEmpty()) {
            mUserPublicData = loadUserPublicData();
            mEnterButton.setVisibility(View.INVISIBLE);
            mExitButton.setVisibility(View.VISIBLE);
        }
    }
    /**
     * метод обработки нажатий на кнопки
     * @param v нажатая кнопка
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            /**
             * обработка нажатия кнопки "Поиск"
             */
            case R.id.search_button:
                mTargetCountry = ((EditText) findViewById(R.id.readnamecountry)).getText().toString();
                if ((mTargetCountry == null) || (mTargetCountry.equals("Страна: не выбрано"))) {
                    Toast.makeText(MainActivity.this,
                            "Нужно выбрать страну",
                            Toast.LENGTH_LONG).show();
                    break;
                }
                for (int i = 0; i < mCountryList.length; i++) {
                    if (mTargetCountry.toLowerCase().equals(mCountryList[i].toLowerCase())) {
                        Toast.makeText(MainActivity.this,
                                "Вы выбрали: " + mTargetCountry,
                                Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(this, VisaListActivity.class);
                        intent.putExtra("target_country", mTargetCountry);
                        startActivity(intent);
                    }
                }

                break;
            /**
             * обработка нажатия кнопки "Вход"
             */
            case R.id.login_button:
                if (mToken.equals("")) {
                    Intent intent1 = new Intent(this,
                            SignInActivity.class);
                    startActivity(intent1);
                } else {
                    Toast.makeText(MainActivity.this,
                            "Вы уже вошли",
                            Toast.LENGTH_LONG).show();
                }
                break;
            /**
             * обработка нажатия кнопки "Личный кабинет"
             */
            case R.id.personal_info_button:
                if (!mToken.equals("")) {
                    Intent intent2 = new Intent(this,
                            AccountActivity.class);
                    startActivity(intent2);
                } else {
                    Toast.makeText(MainActivity.this,
                            "Необходимо войти",
                            Toast.LENGTH_LONG).show();
                }
                break;
            /**
             * обработка нажатия кнопки "Справка"
             */
            case R.id.certificate_button:
                Uri address = Uri.parse("http://www.consultant.ru/");
                Intent openLinkIntent = new Intent(Intent.ACTION_VIEW,
                        address);
                if (openLinkIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(openLinkIntent);
                } else {
                    Toast.makeText(MainActivity.this,
                            "Возникла неполадка с подключением",
                            Toast.LENGTH_LONG).show();
                }

                break;
            /**
             * обработка нажатия кнопки "Полезно: ... "
             */
            case R.id.additional_info_button:
                kindarticle = "Полезные статьи";
                Intent intent66 = new Intent(this,
                       ArticlesActivity.class);
               intent66.putExtra("kindarticle",
                        kindarticle);
                startActivity(intent66);
//                //Toast.makeText(MainActivity.this,
//                "Недоступно. В ближайшем обновлении работа кнопки будет восстановлена",
//                        Toast.LENGTH_LONG).show();
                break;
            /**
             * обработка нажатия кнопки "Интересно: ... "
             */
            case R.id.interests_button:
                kindarticle = "Интересные статьи";
              Intent intent36 = new Intent(this,
                        ArticlesActivity.class);
                intent36.putExtra("kindarticle",
                        kindarticle);
//               intent36.putExtra("idperson",
//                        idperson);
                startActivity(intent36);
//                //Toast.makeText(MainActivity.this,
//                "Недоступно. В ближайшем обновлении работа кнопки будет восстановлена",
//                        Toast.LENGTH_LONG).show();
                break;
            /**
             * обработка нажатия кнопки "Выход"
             */
            case R.id.exit_button:
                logout();
                break;
            default:
                break;
        }
    }
    /**
     * метод загрузки токена
     * @return возвращает токен
     */
    private String loadToken() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        String token = pref.getString("token",
                "");
        return token;
    }
    /**
     * метод загрузки информации о пользователе
     * @return возвращает объект data
     */
    private UserPublicData loadUserPublicData() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        UserPublicData data = new UserPublicData();
        data.mFirstName = pref.getString("first_name",
                "");
        data.mMiddleName = pref.getString("middle_name",
                "");
        data.mLastName = pref.getString("last_name",
                "");
        return data;
    }
    /**
     * метод выхода из системы
     */
    private void logout() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        Toast.makeText(
                this,
                "До встречи, " + mUserPublicData.mFirstName + " !",
        Toast.LENGTH_SHORT)
                .show();
        pref.edit().clear().apply();
        mToken = "";
        mEnterButton.setVisibility(View.VISIBLE);
        mExitButton.setVisibility(View.INVISIBLE);
    }
}
