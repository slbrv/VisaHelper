package com.visahelper.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visahelper.R;
import com.visahelper.auth.AuthApi;
import com.visahelper.auth.SignUpPostData;
import com.visahelper.config.Config;
import com.visahelper.data.UserPublicData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * класс для регистрации в системе
 * @author Nargiz
 * @version 1.0
 */
public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * переменная кнопок зарегистрироваться и возврата на предыдущую страницу
     */
    private Button done, rBackButton;
    /**
     * объект библиотеки для сетевого взаимодействия
     */
    private Retrofit mRetrofit;
    /**
     * переменная интерфейса AuthApi
     */
    private AuthApi mAuthApi;
    /**
     * метод создания компонентов страницы
     * @param savedInstanceState объект, который хранит данные о состоянии
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_str4);
        rBackButton = (Button) findViewById(R.id.rback_button);
        done = findViewById(R.id.signup_button);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mAuthApi = mRetrofit.create(AuthApi.class);

        done.setOnClickListener(this);
        rBackButton.setOnClickListener(this);
    }
    /**
     * метод обработки нажатий на кнопки
     * @param v нажатая кнопка
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * обработка нажатия кнопки "Назад"
             */
            case R.id.rback_button:
                finish();
                break;
            /**
             * обработка нажатия кнопки "Готово!"
             */
            case R.id.signup_button:
                final String firstName =
                        ((EditText) findViewById(R.id.getname)).getText().toString();
                if (firstName.length() < 1) {
                    Toast.makeText(
                            this,
                            "Введите имя !",
                            Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                final String middleName =
                        ((EditText) findViewById(R.id.getmiddlename)).getText().toString();
                if (middleName.length() < 1) {
                    Toast.makeText(
                            this,
                            "Введите отчество!",
                            Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                final String lastName =
                        ((EditText) findViewById(R.id.getlastname)).getText().toString();
                if (lastName.length() < 1) {
                    Toast.makeText(
                            this,
                            "Введите фамилию !",
                            Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                String email = ((EditText) findViewById(R.id.namemail)).getText().toString();
                if (TextUtils.isEmpty(email) ||
                    !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Toast.makeText(
                            this,
                            "Неверно введен email!",
                            Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                String password = ((EditText) findViewById(R.id.password1)).getText().toString();
                if (password.length() < 6) {
                    Toast.makeText(
                            this,
                            "Слишком короткий пароль (Введите не менее 6 символов)!",
                            Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                String verifyPassword = ((EditText) findViewById(R.id.password2)).getText().toString();

                if (password.equals(verifyPassword)) {
                    SignUpPostData data = new SignUpPostData();
                    data.mFirstName = firstName;
                    data.mMiddleName = middleName;
                    data.mLastName = lastName;
                    data.mPassword = password;
                    data.mEMail = email;

                    Call<String> call = mAuthApi.signup(data);
                    call.enqueue(new Callback<String>() {
                        /**
                         * успешная отправка запроса
                         * @param call возврат call объекта
                         * @param response результат будет в объекте response
                         */
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String token = response.body();
                            switch (response.code()) {
                                case 200:
                                    Log.i(Config.APP_TAG, token);
                                    UserPublicData data = new UserPublicData();
                                    data.mFirstName = firstName;
                                    data.mMiddleName = middleName;
                                    data.mLastName = lastName;
                                    saveUserData(token, data);
                                    Toast.makeText(
                                            SignUpActivity.this,
                                            "Добро пожаловать, " + data.mFirstName + "!",
                                            Toast.LENGTH_LONG)
                                            .show();
                                    finish();
                                    break;
                                case 400:
                                    Log.e(Config.APP_TAG, "Sign up status: 400");
                                    Toast.makeText(SignUpActivity.this,
                                            "Пользователь с таким email'ом существует.",
                                            Toast.LENGTH_LONG)
                                            .show();
                                    break;
                                case 500:
                                    Log.e(Config.APP_TAG, "Sign up status: 500");
                                    Toast.makeText(SignUpActivity.this,
                                            "Ошибка регистрации. Проверьте соединение.",
                                            Toast.LENGTH_LONG)
                                            .show();
                                    break;
                            }
                        }
                        /**
                         * ошибка при отправке запроса
                         * @param call возврат call объекта
                         * @param t ошибка
                         */
                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(SignUpActivity.this,
                                    "Ошибка регистрации. Проверьте соединение.",
                                    Toast.LENGTH_LONG)
                                    .show();
                            Log.e(Config.APP_TAG, "Failed sign up");
                            Log.e(Config.APP_TAG, t.toString());
                            finish();
                        }
                    });
                } else {
                    Toast.makeText(
                            SignUpActivity.this,
                            "Введенные пароли не совпадают.",
                            Toast.LENGTH_LONG)
                            .show();
                    //Toast.makeText(str4.this, password1+password2, Toast.LENGTH_LONG).show();
                    break;
                }
                break;

            default:
                break;
        }
    }
    /**
     * метод сохранения данных пользователя
     * @param data объект data
     */
    private void saveUserData(String token, UserPublicData data) {
        SharedPreferences pref = getSharedPreferences("user_data", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("token", token);
        editor.putString("first_name", data.mFirstName);
        editor.putString("middle_name", data.mMiddleName);
        editor.putString("last_name", data.mLastName);
        editor.apply();
        finish();
    }
}