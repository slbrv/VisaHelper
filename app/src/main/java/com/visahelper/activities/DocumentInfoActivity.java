package com.visahelper.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.visahelper.R;

public class DocumentInfoActivity extends AppCompatActivity implements View.OnClickListener {
    TextView printnamevisaandsomewords;
    private Button iBackButton;
    String namevisa = null, namecountry = null;
    int activationOfAccount;
    String[] documents = {"Согласие на обработку персональных данных", "Заявление на получение визы", "Заграничный паспорт", "Цветная фотография", "Квитанция об оплате визового сбора", "Медицинская страховка", "Подтверждение бронирования билетов"};
    String ld1 = "Необходима 1 штука; \n" +
            "- Заполняется в печатном виде. \n" +
            "- п.1 - 3 заполняются буквами латинского алфавита; остальная информация заполняется на выбранном заявителем языке: русском/болгарском/ английском; \n" +
            "- данные заполняются в соответствии с заграничным паспортом. \n" +
            "- пп.19-20 (профессия, место работы) школьники и студенты вписывают данные учебного заведения; дошкольники – ставится прочерк; неработающие - «не работаю», пенсионеры - «пенсионер»; \n" +
            "- в п. 21 указывается «Посещение родственников или друзей», если целью Вашей поездки является посещение своей недвижимости или недвижимости родственников/друзей; \n" +
            "- Номер телефона с кодом государства (п.17, 20, 31, 32); \n" +
            "- Количество дней для двукратной визы (п.25) зависит от основания запроса визы: \n" +
            "• если запрашивается для выезда в третью страну – указываются дни фактического пребывания в Болгарии (п.25), не включая дни пребывания в третьей стране \n" +
            "• если для повторной поездки в Болгарию, то указывается сумма дней по обеим поездкам (п.25). Двукратная виза запрашивается только на 6 месяцев (п.29,30): Пример: запрашивается двукратная виза с датой въезда в Болгарию 30/07/18. В таком случае в п. 29 необходимо указать 30.07.2018, в п. 30 указывается 29.01.2019 \n" +
            "- Количество дней для многократной визы всегда 90 дней (п.25). В п. 29: дата первого въезда по документам; в п. 30 указывается дата: +6 мес/1 год/2 года/3 года к дате первого въезда минус 1 день. Пример: запрашивается многократная годовая виза с датой въезда в Болгарию 30.08.19. В таком случае в п. 29 указывается: 30.08.2019, в п. 30: 29/08/2020. \n" +
            "- Подпись в анкете: \n" +
            "• Для однократной визы подписей должно быть две: в п.37 и в последней графе на 3-ей странице анкеты (вместе с расшифровкой); \n" +
            "• Для двукратной или многократной визы - подписей должно быть три: в п. 37; на третьей странице анкеты после слов: „Мне известно, что я должен обладать подходящей страховкой для поездок за границу...“ и в последней графе на 3-ей странице анкеты; \n" +
            "• За несовершеннолетнего ребенка до 14 лет - расписываются оба родителя или опекуна. \n" +
            "• Для детей от 14 до 18 лет - анкету подписывают оба родителя и ребёнок.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infodocuments);
        iBackButton = (Button) findViewById(R.id.iback_button);

      /*  Intent intent888 = getIntent();
        namecountry = intent888.getStringExtra("countryname1");
        namevisa = intent888.getStringExtra("visaname1");
        activationOfAccount = intent888.getIntExtra("activationOfAccount1", activationOfAccount);

        printnamevisaandsomewords = (TextView) findViewById(R.id.printnamevisaandsomewords);
        printnamevisaandsomewords.setText(String.format("Информация о документах, необходимых для следующей визы: " + namevisa));
       */

        ListView list = (ListView) findViewById(R.id.listdocumentsforinfo);
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, documents);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {   // по позиции получаем выбранный элемент
                String selectedItem = documents[position];
                if (!selectedItem.equals("Не выбрано")) {
                    Toast.makeText(DocumentInfoActivity.this, "Вы выбрали: " + selectedItem, Toast.LENGTH_LONG).show();

                    final Dialog dialog = new Dialog(DocumentInfoActivity.this);
                    dialog.setContentView(R.layout.dialogfordocs_layout);
                    TextView text = (TextView) dialog.findViewById(R.id.description);
                    //тут надо выводить то, что выбрал пльзователь
                    text.setText(ld1);

                    Button button2 = (Button) dialog.findViewById(R.id.button3);
                    button2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    dialog.setCancelable(false);
                    dialog.setTitle("Информация о " + selectedItem);
                    dialog.show();
                }
            }
        });
        iBackButton.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iback_button:
                finish();
                break;
        }
    }
}