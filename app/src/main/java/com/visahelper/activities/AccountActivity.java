package com.visahelper.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visahelper.R;
import com.visahelper.adapters.NumbersAdapter;
import com.visahelper.adapters.RecycleAdapter;
import com.visahelper.config.Config;
import com.visahelper.data.PostPersonalData;
import com.visahelper.data.DocsDataApi;

import java.util.Arrays;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Call;

/**
 * Это класс, который используется для вывода полей персональных данных, их заполнения и отправки
 * @author Nargiz
 * @version 1.1
 */

public class AccountActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * переменная кнопки для возврата на предыдущую страницу
     */
    private Button mBackButton;
    /**
     * переменная кнопки для отправки данных на сервер
     */
    private Button mSendButton;
    /**
     * переменная, хранящая страну
     */
    private String mTargetCountry;
    /**
     * пременная, хранящая id визы
     */
    private int mVisaId;
    /**
     * перменная, хранящая название визы
     */
    private String mVisaName;
    /**
     * компонент пользовательского интерфейса, позволяющий создавать прокручиваемый список
     */
    private RecyclerView mRecyclerView;
    /**
     * используется для отображения списка в особом формате
     */
    private RecyclerView.LayoutManager mLayoutManager;
    /**
     * указывает на класс RecycleAdapter, который необходим для правильной генерации надписей к полям
     */
    private RecycleAdapter mAdapter;
    /**
     * объект библиотеки для сетевого взаимодействия
     */
    private Retrofit mRetrofit;
    /**
     * переменная, указывающая на класс, в котором описано, какой .php файл нужно использовать
     */
    private DocsDataApi mDocsDataApi;
    /**
     * переменная, хранящая токен пользователя
     */
    private String mToken;

    /**
     * метод используется для создания кнопок, получения данных о пользователе, выводе полей для
     * заполнения персональных данных в зависимости от выбранной визы
     * @param savedInstanceState объект, который хранит данные о состоянии
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_str5);
        mBackButton = (Button) findViewById(R.id.back_button);
        mSendButton = (Button) findViewById(R.id.send_button);

        Intent intent = getIntent();
        mTargetCountry = intent.getStringExtra("target_country");
        mVisaId = intent.getIntExtra("visa_id", 0);
        mVisaName = intent.getStringExtra("visa_name");

        mSendButton.setOnClickListener(this);
        mBackButton.setOnClickListener(this);

        mRecyclerView = findViewById(R.id.rv_numbers);

        mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecycleAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mDocsDataApi = mRetrofit.create(DocsDataApi.class);

        mToken = loadToken();
        /**
         * если пользователь хочет оформить визу
         */
        if(mVisaId != 0) {
            Call<List<Integer>> call = mDocsDataApi.getAnkData(mToken, mVisaId);
            call.enqueue(new Callback<List<Integer>>() {
                /**
                 * успешная отправка запроса
                 * @param call возврат call объекта
                 * @param response результат будет в объекте response
                 */
                @Override
                public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                    if(response.isSuccessful())
                    {
                        Log.i(Config.APP_TAG, "Ok");
                        int[] personalDataTable = new int[87];
                        for (int i = 0; i < personalDataTable.length; i++) {
                            personalDataTable[i] = response.body().get(i);
                        }
                        mAdapter.setData(personalDataTable);
                    }
                    else
                        Log.e(Config.APP_TAG, "Not successful");
                }

                /**
                 * ошибка при отправке запроса
                 * @param call возврат call объекта
                 * @param t ошибка
                 */
                @Override
                public void onFailure(Call<List<Integer>> call, Throwable t) {
                    Log.e(Config.APP_TAG, "Error: " + t);
                }
            });
            /**
             * если пользователь хочет перейти в личный кабинет
             */
        } else {
            int[] personalDataTable = new int[87];
            Arrays.fill(personalDataTable, 2);
            mAdapter.setData(personalDataTable);
        }

        if (!mToken.isEmpty()) {
            Call<List<String>> call = mDocsDataApi.getPersonalData(mToken);
            call.enqueue(new Callback<List<String>>() {
                /**
                 * успешная отправка запроса
                 * @param call call возврат call объекта
                 * @param response результат будет в обекте response
                 */
                @Override
                public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                    if (response.isSuccessful() && response.body().size() > 0) {
                        mAdapter.setData(response.body());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        Log.e(Config.APP_TAG, "Error: token is empty");
                    }
                }
                /**
                 * ошибка при отправке запроса
                 * @param call возврат call объекта
                 * @param t ошибка
                 */
                @Override
                public void onFailure(Call<List<String>> call, Throwable t) {
                    Log.e(Config.APP_TAG, "Error: cannot load docs data. \n" + t);
                }
            });
        } else {
            Log.e(Config.APP_TAG, "Error: token is empty");
            Toast.makeText(this,
                    "Ошибка соединения. Попробуйте позже.",
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    /**
     * метод обработки нажатий на кнопки
     * @param v нажатая кнопка
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * нажатие кнопки "назад"
             */
            case R.id.back_button:
                finish();
                break;
            /**
             * нажатие кнопки отправить
             */
            case R.id.send_button:
                final String result = ((RecycleAdapter) mRecyclerView.getAdapter()).getResult();
                Log.i(Config.APP_TAG, "VisaID: " + mVisaId
                        + ". Visa name: " + mVisaName
                        + ". Target country: " + mTargetCountry
                        + ". Result: " + result);
                if (!mToken.isEmpty()) {
                    PostPersonalData post = new PostPersonalData();
                    post.mToken = mToken;
                    post.mVisaId = mVisaId;
                    post.mData = result;
                    Call<Void> call = mDocsDataApi.postPersonalData(post);
                    call.enqueue(new Callback<Void>() {
                        /**
                         * успешная отправка запроса на сохранение данных в БД
                         * @param call call возврат call объекта
                         * @param response результат будет в обекте response
                         */
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.isSuccessful()) {
                                Log.i(Config.APP_TAG, "Ok");
                            } else {
                                Log.e(Config.APP_TAG, "Error: token is empty");
                            }
                        }

                        /**
                         * ошибка при отправке запроса
                         * @param call возврат call объекта
                         * @param t ошибка
                         */
                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            Log.e(Config.APP_TAG, "Error: cannot save docs data. \n" + t);
                        }
                    });
                } else {
                    Log.e(Config.APP_TAG, "Error: token is empty");
                    Toast.makeText(this,
                            "Ошибка соединения. Попробуйте позже.",
                            Toast.LENGTH_LONG)
                            .show();
                }
                finish();
                break;
            default:
                break;
        }
    }

    /**
     * загрузка токена
     * @return возвращает токен пользователя
     */
    private String loadToken() {
        SharedPreferences pref = getSharedPreferences("user_data",
                MODE_PRIVATE);
        String token = pref.getString("token",
                "");
        return token;
    }
}