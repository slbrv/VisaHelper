package com.visahelper.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visahelper.R;
import com.visahelper.auth.AuthApi;
import com.visahelper.auth.SignInPostData;
import com.visahelper.config.Config;
import com.visahelper.data.DataHolderApi;
import com.visahelper.data.UserPublicData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * класс для авторизации в системе
 * @author Nargiz
 * @version 1.0
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * переменная кнопок входа и возврата на предыдущую страницу
     */
    private Button move4btn2, sBackButton;
    /**
     * переменная кнопки перехода на страницу регистрации
     */
    private Button move3devide2or1btn2;
    /**
     * объект библиотеки для сетевого взаимодействия
     */
    private Retrofit mRetrofit;
    /**
     * переменная интерфейса AuthApi
     */
    private AuthApi mAuthApi;
    /**
     * переменная интерфейса DataHolderApi
     */
    private DataHolderApi mDataHolderApi;

    /**
     * метод создания компонентов страницы
     * @param savedInstanceState объект, который хранит данные о состоянии
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_str6);
        sBackButton = (Button) findViewById(R.id.sback_button);
        move4btn2 = findViewById(R.id.signup_button);
        move3devide2or1btn2 = findViewById(R.id.login_button);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        mAuthApi = mRetrofit.create(AuthApi.class);
        mDataHolderApi = mRetrofit.create(DataHolderApi.class);

        move4btn2.setOnClickListener(this);
        move3devide2or1btn2.setOnClickListener(this);
        sBackButton.setOnClickListener(this);
    }
    /**
     * активность получает передний план
     */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences pref = getSharedPreferences("user_data", MODE_PRIVATE);
        String token = pref.getString("token", "");
        if (!token.isEmpty())
            finish();
    }
    /**
     * метод обработки нажатий на кнопки
     * @param v нажатая кнопка
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * обработка нажатия кнопки "Назад"
             */
            case R.id.sback_button:
                finish();
                break;
            /**
             * обработка нажатия кнопки "Регистрация"
             */
            case R.id.signup_button:
                Intent intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                break;
            /**
             * обработка нажатия кнопки "Вход"
             */
            case R.id.login_button:
                String email = ((EditText) findViewById(R.id.email)).getText().toString();
                String password = ((EditText) findViewById(R.id.password)).getText().toString();

                SignInPostData data = new SignInPostData();
                data.mEmail = email;
                data.mPassword = password;

                Call<String> call = mAuthApi.signin(data);
                call.enqueue(new Callback<String>() {
                    /**
                     * успешная отправка запроса
                     * @param call возврат call объекта
                     * @param response результат будет в объекте response
                     */
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String token = response.body();
                        switch (response.code()) {
                            case 200:
                                Log.i(Config.APP_TAG, token);
                                getUserPublicData(token);
                                break;
                            case 400:
                                Log.e(Config.APP_TAG, "Sign up status: 400");
                                Toast.makeText(
                                        SignInActivity.this,
                                        "Ошибка авторизации. Неверная почта или пароль.",
                                        Toast.LENGTH_LONG)
                                        .show();
                                break;
                            case 500:
                                Log.e(Config.APP_TAG, "Sign up status: 500");
                                Toast.makeText(
                                        SignInActivity.this,
                                        "Ошибка авторизации. Проверьте соединение.",
                                        Toast.LENGTH_LONG)
                                        .show();
                                break;
                        }
                    }
                    /**
                     * ошибка при отправке запроса
                     * @param call возврат call объекта
                     * @param t ошибка
                     */
                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(
                                SignInActivity.this,
                                "Ошибка авторизации. Проверьте соединение.",
                                Toast.LENGTH_SHORT)
                                .show();
                        Log.e(Config.APP_TAG, t.toString());
                    }
                });
                break;
            default:
                break;
        }
    }

    /**
     * метод загрузки информации о пользователе
     * @param token токен пользователя
     */
    private void getUserPublicData(final String token) {
        Call<UserPublicData> call = mDataHolderApi.getUserPublicData(token);
        call.enqueue(new Callback<UserPublicData>() {
            /**
             * успешная отправка запроса
             * @param call возврат call объекта
             * @param response результат будет в объекте response
             */
            @Override
            public void onResponse(Call<UserPublicData> call, Response<UserPublicData> response) {
                if (response.isSuccessful()) {
                    switch (response.code()) {
                        case 200:
                            UserPublicData data = response.body();
                            saveToken(token);
                            saveUserPublicData(data);
                            Toast.makeText(
                                    SignInActivity.this,
                                    "Добро пожаловать, " + data.mFirstName + "!",
                                    Toast.LENGTH_LONG)
                                    .show();
                            break;
                        case 400:
                            Log.e(Config.APP_TAG, "Get user public data status: 400");
                            break;
                        case 500:
                            Log.e(Config.APP_TAG, "Get user public data status: 500");
                            break;
                    }
                }
                finish();
            }
            /**
             * ошибка при отправке запроса
             * @param call возврат call объекта
             * @param t ошибка
             */
            @Override
            public void onFailure(Call<UserPublicData> call, Throwable t) {
                Toast.makeText(
                        SignInActivity.this,
                        "Проблемы с подключением, проверьте соединение.",
                        Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    /**
     * метод сохранения токена пользователя
     * @param token токен пользователя
     */
    private void saveToken(String token) {
        SharedPreferences pref = getSharedPreferences("user_data", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("token", token);
        editor.apply();
    }

    /**
     * метод сохранения данных пользователя
     * @param data объект data
     */
    private void saveUserPublicData(UserPublicData data) {
        SharedPreferences pref = getSharedPreferences("user_data", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("first_name", data.mFirstName);
        editor.putString("middle_name", data.mMiddleName);
        editor.putString("last_name", data.mLastName);
        editor.apply();
    }
}
