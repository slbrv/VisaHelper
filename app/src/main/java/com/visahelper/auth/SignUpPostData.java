package com.visahelper.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignUpPostData {
    @SerializedName("email")
    public String mEMail;
    @SerializedName("password")
    public String mPassword;
    @SerializedName("first_name")
    public String mFirstName;
    @SerializedName("last_name")
    public String mLastName;
    @SerializedName("middle_name")
    public String mMiddleName;
}
