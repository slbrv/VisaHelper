package com.visahelper.auth;

import com.google.gson.annotations.SerializedName;

public class SignInPostData {
    @SerializedName("email")
    public String mEmail;
    @SerializedName("password")
    public String mPassword;
}
