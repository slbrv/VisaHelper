package com.visahelper.auth;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AuthApi {
    @POST("post_signup.php")
    Call<String> signup(@Body SignUpPostData data);

    @POST("post_signin.php")
    Call<String> signin(@Body SignInPostData data);
}
