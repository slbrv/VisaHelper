package com.visahelper.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.visahelper.R;
import com.visahelper.activities.DocumentsListActivity;
import com.visahelper.data.DocsDataApi;

import retrofit2.Retrofit;

public class NumbersAdapter extends RecyclerView.Adapter<NumbersAdapter.NumberViewHolder> {
    private int numberItems;
    private static int viewHolderCount;
    private int[] mData;

    public NumbersAdapter(int numberOfItems, int[] data) {
        numberItems = numberOfItems;
        viewHolderCount = 0;
        mData = data;
    }

    @NonNull
    @Override
    public NumberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        //String[] hint=new String[100];
        //String[] namepersonaldoc=new String[100];
        int layuoutIdFoeListItem = R.layout.ld_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layuoutIdFoeListItem, parent, false);
        NumberViewHolder viewHolder = new NumberViewHolder(view, mData);

        //viewHolder.viewHolderIndex.setHint(String.format(hint[numberItems]));
        //viewHolder.viewHolderIndex.setText(" ");
        //namepersonaldoc[numberItems]=((EditText)viewHolder.viewHolderIndex).getText().toString();

        viewHolderCount++;
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NumberViewHolder holder, int position) {
        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return numberItems;
    }


    class NumberViewHolder extends RecyclerView.ViewHolder {
        EditText viewHolderIndex;
        TextView listItemNumberView;
        public String[] hint = new String[100];
        public String[] namepersonalinfo = new String[100];
        public int[] needpersonalinfo = new int[138];


        public NumberViewHolder(@NonNull View itemView, int[] data) {
            super(itemView);
            listItemNumberView = itemView.findViewById(R.id.necessarily);
            viewHolderIndex = itemView.findViewById(R.id.personal_doc);
            needpersonalinfo = data;
        }

        void bind(int listIndex) {
            for (int i = 0; i < 100; i++) {
                hint[i] = "Подсказка№ " + i;
            }

            if (needpersonalinfo[listIndex] == 1) {
                listItemNumberView.setText(listIndex + "\nОбязательно");
            }
            if (needpersonalinfo[listIndex] == 0) {
                listItemNumberView.setText(listIndex + "\nНе обязательно");
            }
            if (namepersonalinfo[listIndex] != null) {
                listItemNumberView.setText(listIndex + "\nУже заполнено");
            }

            viewHolderIndex.setHint(String.format(hint[listIndex]));
            namepersonalinfo[listIndex] = ((EditText) viewHolderIndex).getText().toString();
            //отправка в базу данных. сразу

            /*
            Intent intent2 = new Intent();
            intent2.putExtra("personalinformation", namepersonalinfo);
            intent2.setClass(NumbersAdapter, str3divede2.class);
            startActivity(intent2);*/
            viewHolderIndex.setText(String.format(""));
            viewHolderIndex.setHint(String.format(hint[listIndex]));
        }

        public void main(String args[]) {
            // NumbersAdapter class1 = new NumberViewHolder(View itemView);
            DocumentsListActivity class2 = new DocumentsListActivity();
/*
            String id = class1.getId();
            class2.setId(id);*/
        }
    /*
    static class Translation {
        public static void main(String args[]) {
            NumbersAdapter class1 = new NumberViewHolder();
            Str2 class2 = new Str2();

            String id = class1.getId();
            class2.setId(id);
        }*/


    }
}
