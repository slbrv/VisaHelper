package com.visahelper.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.visahelper.R;
import com.visahelper.activities.AccountActivity;
import com.visahelper.activities.ArticlesActivity;
import com.visahelper.config.Config;

import java.util.List;

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ImageViewHolder> {
    private final AccountActivity context;
    private int[] needpersonaldata = new int[87];
    String[] personaldata = new String[87];
    String[] personaldataforBD = new String[138];
    private String mResult;
    //private int[] publicatepersonaldoc={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};


    public RecycleAdapter(AccountActivity context) {
        this.context = context;
    }

    public void setData(int[] data) {
        this.needpersonaldata = data;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.albom_layout, parent, false);
        ImageViewHolder imageViewHolder = new ImageViewHolder(view);

        return imageViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageViewHolder holder, final int position) {
        holder.needdatatext.setText(personaldata[position]);
        int[] needforthisvisadoc = new int[137];
        //примечание: поля [лд33, лд36}а также [69.70] заполняются по определению в макросах значением "нет"
        for (int i = 0; i < 137; i++) {
            needforthisvisadoc[i] = 1;
        }

        final String[] personaldataanddescription = {"Фамилия", "Фамилия по рождению", "Имя",
                "Дата рождения", "Место рождения", "Государство рождения",
                "Настоящее гражданство", "Гражданство по рождению", "Пол",
                "Семейное положение", "ФИО, адрес, гражданство опекуна", "Национальный идентификационный номер",
                "Вид документа для заграничной поездки", "Номер документа для заграничной поездки", "Дата выдачи документа для заграничной поездки",
                "Срок действия документа для заграничной поездки", "Кем выдан документ для заграничной поездки", "Домашний адрес и электронный адрес кандидата",
                "Номер телефона", "Актуальная профессия", "Адрес и телефон работодателя/адрес учебного учреждения",
                "Основание для поездки", "Государство первого въезда", "Город назначения",
                "Город, являющийся целью поездки", "Число запрашиваемых въездов", "продолжительность пребывания или транзитного проезда",
                "Выдача шенгенской визы за последние 3 года", "С какого действует Шенгенская виза(Если была за последние 3 года)", "По какое действует Шенгенская виза(Если была за последние 3 года)",
                "Предыдущее снятие отпечатка пальца для подачи заявления на Шенгенскую визу", "Дата снятия отпечатка пальцев для подачи заявления на Шенгенскую визу (Если происходило)", "Разрешение не въезд в гос-во, являющейся целью поездки(Если есть)",
                "Дата начала действия разрешение не въезд в гос-во, являющейся целью поездки(Если есть)", "Дата окончания действия разрешение не въезд в гос-во, являющейся целью поездки(Если есть)", "Номер разрешения на воссоединения с семьей(Если есть)",
                "Информация о едином окне, выдавшем разрешение о воссоединении с семьей (Если есть)", "Дата начала действия разрешения на воссоединения с семьей(Если есть)", "Дата окончания действия разрешения на воссоединения с семьей(Если есть)",
                "Планируемая дата въезда в страну", "Планируемая дата выезда из страны", "Фамилия имя приглашающего лица в стране ИЛИ наименование гостиниц ИЛИ временный адрес в стране",
                "Имя и Фамилия лица, запросившего визу ИЛИ адрес проживания (СМ пояснение)", "Адрес ИЛИ электр. адрес приглашающего лица или гостиницы ИЛИ временный адрес(Если есть)", "телефон/факс приглашающей стороны (если есть)",
                "Название и адрес приглашающей компании/организации(Если есть)", "Телефон/факс(Если есть)", "Адрес проживания и адрес эл. почты лица, запросившего визу на воссоединение семьи или Работодателя(Если есть)",
                "Имя, фамилия, адрес проживания, номер телефона/ факса контакного лица(Если есть)", "Обеспечение расходов на поезку", "Вид средств на содержания наличные деньги(Если сам)",
                "Вид средств на содержания дорожные чеки(Если сам)", "Вид средств на содержания кредитная карта(Если сам)", "Место проживания предоплачено(Если сам)",
                "Транспорт предоплачен(Если сам)", "Иные средства для расчета(Если сам)", "Укажите спонсора(Если есть)",
                "Иные приглашающие лица (Если есть неупомянутые ранее)", "Вид средств на содержания наличные деньги(Если спонсор)", "Место проживания предоплачено(Если спонсор)",
                "Все расходы во время пребывания оплачены спонсором", "Транспорт предоплачен(Если спонсор)", "Иные средства для расчета(Если спонсор)",
                "Фамилия члена семьи, являющегося гражданином ЕС, ЕЭП или Швейцарии", "Имя члена семьи, являющегося гражданином ЕС, ЕЭП или Швейцарии", "Дата рождения члена семьи, являющегося гражданином ЕС, ЕЭП или Швейцарии",
                "Гражданство члена семьи, являющегося гражданином ЕС, ЕЭП или Швейцарии", "Номер документа члена семьи, являющегося гражданином ЕС, ЕЭП или Швейцарии", "Степень родства с гражданином ЕС, ЕОП или Швейцарии",
                "Место подачи анкеты", "Дата подачи анкеты", "День подачи анкет",
                "Месяц подачи анкет(словом)", "год подачи анкет(две цифры)", "Отчество(если есть)",
                "Вид основного документа", "Серия основного документа", "Номер основного документа",
                "Кем и когда выдан основной документ", "Адрес проживания", "ФИО представителя(Если есть)",
                "Вид основного документа представителя(Если есть)", "Серия основного документа представителя(Если есть)", "Номер основного документа представителя(Если есть)",
                "Кем и когда выдан основной документ представителя(Если есть)", "Адрес проживания представителя(Если есть)", "Реквезиты доверенности представителя(Если есть)"};


        final String[] descriptionofpersonaldata = {"Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Возможные варианты: М/Ж",
                "Возможные варианты: женат/замужем, живу отдельно, неженат/незамужем, разведеный(-ая), вдовец/вдова, другое(свой вариант)", "Для несовершеннолетних", "Если таковой имеется",
                "обычный паспорт,дипломатический паспорт, служебный паспорт, другое(свой вариант)", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Отделяется электронный адрес от физического знаком ;.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Если есть работа, то нужно написать: адрес и телефон работодателя. Если студент, то: адрес учебного учреждения. Если пенсонер, то: пенсионер. Если безработный, то: безработный",
                "возможные варианты: мед. причины, служебная поездка, спорт, обучение, другое", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Однократно/двукратно/многократно.", "Укажите число дней",
                "Возможные варианты: нет/да.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "нет/да", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Информация о едином окне, выдавшем разрешение о воссоединении с семьей", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Имя и фамилия лица, запросившего визу на воссоединение семьи или работодателя. В случае визы на усыновление, религиозные цели, работу в области спорта, учебу, служебные цели: адрес пр оживания в Италии", "Если заполняется, то обязательно нужно заполнить и следующее поле", "Если заполняется, то и предыдущее поле тоже",
                "Если заполняется, то обязательно нужно заполнить и следующее поле", "Если заполняется, то и предыдущее поле тоже", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Варианты: самостоятельно/есть спонсор", "Возможны варианты: да/нет",
                "Возможны варианты: да/нет", "Возможны варианты: да/нет", "Возможны варианты: да/нет",
                "Возможны варианты: да/нет", "Укажите иные средства", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Возможны варианты: да/нет", "Возможны варианты: да/нет",
                "Возможны варианты: да/нет", "Возможны варианты: да/нет", "Пояснение к данному полю отсутствует.",
                "Личные данные члена семьи, являющегося гражданином Европейского Союза, Европейского Экономического Пространства или Швейцарии", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Возможны варианты:супруга/супруг, ребенок, родственник по восх. линии, иждивенец",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Если есть представитель и в случае получения согласия от представителя субъекта персональных данных",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.",
                "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует.", "Пояснение к данному полю отсутствует."};

        //нумерация всех массивов начинается с 0
        if (needforthisvisadoc[20] == 1) {
            descriptionofpersonaldata[11] = descriptionofpersonaldata[11] + " официальный паспорт";
        }
        if (needforthisvisadoc[22] == 1) {
            descriptionofpersonaldata[11] = descriptionofpersonaldata[11] + " специальный паспорт";
        }

        if (needforthisvisadoc[38] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " туризм";
        }
        if (needforthisvisadoc[39] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " культура";
        }
        if (needforthisvisadoc[44] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " транзитный поезд";
        }
        if (needforthisvisadoc[45] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " посещение родственников или друзей";
        }
        if (needforthisvisadoc[46] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " официальное посещение";
        }
        if (needforthisvisadoc[47] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " аэропортный транзит";
        }
        if (needforthisvisadoc[50] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " воссоединение семьи/сопровождение члена семьи";
        }
        if (needforthisvisadoc[51] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " религиозная";
        }
        if (needforthisvisadoc[52] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " служебная";
        }
        if (needforthisvisadoc[53] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " усыновление";
        }
        if (needforthisvisadoc[54] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " предприниматльская работа";
        }
        if (needforthisvisadoc[55] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + " работа по найму";
        }
        if (needforthisvisadoc[56] == 1) {
            descriptionofpersonaldata[18] = descriptionofpersonaldata[18] + "дипломатическая";
        }

        if (needforthisvisadoc[115] == 1) {
            descriptionofpersonaldata[68] = descriptionofpersonaldata[68] + " ребенок(сын/дочь)";
        }
        if (needforthisvisadoc[116] == 1) {
            descriptionofpersonaldata[68] = descriptionofpersonaldata[68] + " внук";
        }
        if (needforthisvisadoc[117] == 1) {
            descriptionofpersonaldata[68] = descriptionofpersonaldata[68] + " иные";
        }


        //holder.Album.setImageResource(needpersonaldata_id);

        if (needpersonaldata[position] == 0) {
            holder.needdatatext.setText(position + 1 + ". Поле " + personaldataanddescription[position].toLowerCase() + " НЕ обязательно для заполнения");
        } else if (needpersonaldata[position] == 1) {
            holder.needdatatext.setText(position + 1 + ". Поле " + personaldataanddescription[position].toLowerCase() + " обязательно для заполнения");
        } else if (needpersonaldata[position] == 2) {
            holder.needdatatext.setText(position + 1 + ". Поле " + personaldataanddescription[position].toLowerCase());
        }
        holder.AlbumTitle.setHint(personaldataanddescription[position]);
        holder.AlbumTitle.setText(personaldata[position]);
        //personaldata[position]=holder.AlbumTitle.getText().toString();


//функция ниже вылетает, но на нажатие реагирует
        /*
        holder.AlbumTitle.setOnEditorActionListener( new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if( (event.getAction() == KeyEvent.ACTION_DOWN) && event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                    // обработка нажатия Enter
                    //personaldata[position]=holder.AlbumTitle.getText().toString();
                    Toast.makeText(context, "Считалось", Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });*/




/*
        holder.AlbumTitle.setOnKeyListener(new View.OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    // сохраняем текст, введенный до нажатия Enter в переменную
                    personaldata[position]=holder.AlbumTitle.getText().toString();
                    return true;
                 }
                    return false;
             }
            }
        );
        */
/*
        holder.AlbumTitle.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId==EditorInfo.IME_ACTION_DONE){
                    personaldata[position]=holder.AlbumTitle.getText().toString();
                    return true;
                }
                return false;
            }

        });*/


/*
        View.OnKeyListener enter = new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if(event.getAction() == KeyEvent.ACTION_DOWN &&
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    // сохраняем текст, введенный до нажатия Enter в переменную
                    personaldata[position]=holder.AlbumTitle.getText().toString();

                    //dialogans();
                    return true;
                }
                return false;
            }
        };

        holder.AlbumTitle.setOnKeyListener(enter);*/

/*
        holder.AlbumTitle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId ==  EditorInfo.IME_ACTION_NEXT) {
                    personaldata[position]=holder.AlbumTitle.getText().toString();
                    return true;


                if(event.getAction() == KeyEvent.ACTION_DOWN &&event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
                {
                    personaldata[position]=holder.AlbumTitle.getText().toString();
                    return true;

                }
                return false;
            }
        });*/

/*
        holder.AlbumTitle.setOnKeyListener((View.OnKeyListener) this);

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == 66) {
            Toast.makeText(context, "Enter was pressed", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }*/


        holder.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personaldata[position] = holder.AlbumTitle.getText().toString();
                Toast.makeText(context, "Изменение сохранено", Toast.LENGTH_SHORT).show();

                int i = 0;

                for (i = 0; i < 8; i++) {
                    personaldataforBD[i] = personaldata[i];
                }
                //Toast.makeText(context, personaldata[8].toLowerCase(), Toast.LENGTH_SHORT).show();


                if (personaldata[i] != null && personaldata[i].toLowerCase().equals("м")) {
                    personaldataforBD[8] = "1";
                    personaldataforBD[9] = "0";
                }
                if (personaldata[i] != null && personaldata[i].toLowerCase().equals("ж")) {
                    personaldataforBD[8] = "0";
                    personaldataforBD[9] = "1";
                }
                i++;
                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("женат") || personaldata[i].toLowerCase().equals("замужем")) {
                        personaldataforBD[10] = "1";
                        personaldataforBD[11] = "0";
                        personaldataforBD[12] = "0";
                        personaldataforBD[13] = "0";
                        personaldataforBD[14] = "0";
                        personaldataforBD[15] = "0";
                        personaldataforBD[16] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("живу отдельно")) {
                        personaldataforBD[10] = "0";
                        personaldataforBD[11] = "1";
                        personaldataforBD[12] = "0";
                        personaldataforBD[13] = "0";
                        personaldataforBD[14] = "0";
                        personaldataforBD[15] = "0";
                        personaldataforBD[16] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("неженат") || personaldata[i].toLowerCase().equals("незамужем")) {
                        personaldataforBD[10] = "0";
                        personaldataforBD[11] = "0";
                        personaldataforBD[12] = "1";
                        personaldataforBD[13] = "0";
                        personaldataforBD[14] = "0";
                        personaldataforBD[15] = "0";
                        personaldataforBD[16] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("разведенная") || personaldata[i].toLowerCase().equals("разведенный") || personaldata[i].toLowerCase().equals("разведенн") || personaldata[i].toLowerCase().equals("разведена")) {
                        personaldataforBD[10] = "1";
                        personaldataforBD[11] = "0";
                        personaldataforBD[12] = "0";
                        personaldataforBD[13] = "0";
                        personaldataforBD[14] = "0";
                        personaldataforBD[15] = "0";
                        personaldataforBD[16] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("вдова") || personaldata[i].toLowerCase().equals("вдовец")) {
                        personaldataforBD[10] = "0";
                        personaldataforBD[11] = "0";
                        personaldataforBD[12] = "0";
                        personaldataforBD[13] = "0";
                        personaldataforBD[14] = "1";
                        personaldataforBD[15] = "0";
                        personaldataforBD[16] = "0";
                    } else {
                        personaldataforBD[10] = "0";
                        personaldataforBD[11] = "0";
                        personaldataforBD[12] = "0";
                        personaldataforBD[13] = "0";
                        personaldataforBD[14] = "0";
                        personaldataforBD[15] = "1";
                        personaldataforBD[16] = personaldata[i];
                    }
                }
                i++;
                personaldataforBD[17] = personaldata[i];
                i++;
                personaldataforBD[18] = personaldata[i];
                i++;
                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("обычный паспорт")) {
                        personaldataforBD[19] = "1";
                        personaldataforBD[20] = "0";
                        personaldataforBD[21] = "0";
                        personaldataforBD[22] = "0";
                        personaldataforBD[23] = "0";
                        personaldataforBD[24] = "0";
                        personaldataforBD[25] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("официальный паспорт")) {
                        personaldataforBD[19] = "0";
                        personaldataforBD[20] = "1";
                        personaldataforBD[21] = "0";
                        personaldataforBD[22] = "0";
                        personaldataforBD[23] = "0";
                        personaldataforBD[24] = "0";
                        personaldataforBD[25] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("дипломатический паспорт")) {
                        personaldataforBD[19] = "0";
                        personaldataforBD[20] = "0";
                        personaldataforBD[21] = "1";
                        personaldataforBD[22] = "0";
                        personaldataforBD[23] = "0";
                        personaldataforBD[24] = "0";
                        personaldataforBD[25] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("специальный паспорт")) {
                        personaldataforBD[19] = "0";
                        personaldataforBD[20] = "0";
                        personaldataforBD[21] = "0";
                        personaldataforBD[22] = "1";
                        personaldataforBD[23] = "0";
                        personaldataforBD[24] = "0";
                        personaldataforBD[25] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("служебный паспорт")) {
                        personaldataforBD[19] = "0";
                        personaldataforBD[20] = "0";
                        personaldataforBD[21] = "0";
                        personaldataforBD[22] = "0";
                        personaldataforBD[23] = "1";
                        personaldataforBD[24] = "0";
                        personaldataforBD[25] = "0";
                    } else {
                        personaldataforBD[19] = "0";
                        personaldataforBD[20] = "0";
                        personaldataforBD[21] = "0";
                        personaldataforBD[22] = "0";
                        personaldataforBD[23] = "0";
                        personaldataforBD[24] = "1";
                        personaldataforBD[25] = personaldata[i];
                    }
                }
                for (i = 13; i < 18; i++) {
                    personaldataforBD[i + 11] = personaldata[i];
                }
                for (i = 18; i < 22; i++) {
                    personaldataforBD[i + 11] = "1";
                }
                i = 20;
                personaldataforBD[i + 17] = personaldata[i];
                i++;
                personaldataforBD[i + 17] = personaldata[i];
                i++;
                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("туризм")) {
                        personaldataforBD[38] = "1";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("культура")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "1";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("мед. причины") || personaldata[i].toLowerCase().equals("медицинские причины") || personaldata[i].toLowerCase().equals("мед причины") || personaldata[i].toLowerCase().equals("здоровье")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "1";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("служебная поездка")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "1";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("спорт")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "1";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("обучение")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "1";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("транзитный проезд")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "1";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("посещение родственников") || personaldata[i].toLowerCase().equals("посещение друзей") || personaldata[i].toLowerCase().equals("посещение родственников или друзей")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "1";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("официальное посещение")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "1";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("аэропортный транзит")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "1";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("воссоединение семьи") || personaldata[i].toLowerCase().equals("сопровождение члена семьи")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "1";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("религиозная")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "1";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("служебная")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "1";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("усыновление")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "1";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("предпринимательская работа") || personaldata[i].toLowerCase().equals("предпринимательская деятельность")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "1";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("работа по найму")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "1";
                        personaldataforBD[56] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("дипломатическая")) {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "0";
                        personaldataforBD[49] = "0";
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "1";
                    } else {
                        personaldataforBD[38] = "0";
                        personaldataforBD[39] = "0";
                        personaldataforBD[40] = "0";
                        personaldataforBD[41] = "0";
                        personaldataforBD[42] = "0";
                        personaldataforBD[43] = "0";
                        personaldataforBD[44] = "0";
                        personaldataforBD[45] = "0";
                        personaldataforBD[46] = "0";
                        personaldataforBD[47] = "0";
                        personaldataforBD[48] = "1";
                        personaldataforBD[49] = personaldata[i];
                        personaldataforBD[50] = "0";
                        personaldataforBD[51] = "0";
                        personaldataforBD[52] = "0";
                        personaldataforBD[53] = "0";
                        personaldataforBD[54] = "0";
                        personaldataforBD[55] = "0";
                        personaldataforBD[56] = "0";
                    }
                }
                i = 22;
                for (i = 22; i < 25; i++) {
                    personaldataforBD[i + 35] = personaldata[i];
                }
                i = 25;

                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("однократно")) {
                        personaldataforBD[61] = "1";
                        personaldataforBD[62] = "0";
                        personaldataforBD[63] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("двукратно")) {
                        personaldataforBD[61] = "0";
                        personaldataforBD[62] = "1";
                        personaldataforBD[63] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("многократно")) {
                        personaldataforBD[61] = "0";
                        personaldataforBD[62] = "0";
                        personaldataforBD[63] = "1";
                    }
                }
                personaldataforBD[63] = personaldata[26];
                i = 27;
                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("нет")) {
                        personaldataforBD[64] = "1";
                        personaldataforBD[65] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("да")) {
                        personaldataforBD[64] = "0";
                        personaldataforBD[65] = "1";
                    }
                }
                i++;
                personaldataforBD[i + 38] = personaldata[i];
                i++;
                personaldataforBD[i + 38] = personaldata[i];
                i++;
                personaldataforBD[i + 38] = "1";
                i++;
                personaldataforBD[i + 38] = "1";
                i++;

                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("нет")) {
                        personaldataforBD[70] = "1";
                        personaldataforBD[71] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("да")) {
                        personaldataforBD[70] = "0";
                        personaldataforBD[71] = "1";
                    }
                }
                i++;
                for (i = 32; i < 49; i++) {
                    personaldataforBD[i + 40] = personaldata[i];
                }

                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("сам")) {
                        personaldataforBD[90] = "1";
                        personaldataforBD[91] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("спонсор")) {
                        personaldataforBD[90] = "0";
                        personaldataforBD[91] = "1";
                    }
                }
                i++;

                for (i = 51; i < 55; i++) {
                    if ((personaldata[i] != null) && personaldata[i].toLowerCase().equals("да")) {
                        personaldataforBD[i + 43] = "1";
                    } else {
                        personaldataforBD[i + 43] = "0";
                    }
                }

                if (personaldata[i] != null) {
                    personaldataforBD[97] = "1";
                    personaldataforBD[98] = personaldata[i];
                }
                i++;
                personaldataforBD[99] = personaldata[57];
                i++;
                if (personaldata[i] != null) {
                    personaldataforBD[100] = "1";
                    personaldataforBD[101] = personaldata[i];
                }
                i++;

                for (i = 59; i < 63; i++) {
                    if ((personaldata[i] != null) && personaldata[i].toLowerCase().equals("да")) {
                        personaldataforBD[i + 43] = "1";
                    } else {
                        personaldataforBD[i + 43] = "0";
                    }
                }
                i = 63;

                if (personaldata[i] != null) {
                    personaldataforBD[106] = "1";
                    personaldataforBD[107] = personaldata[i];
                }
                i++;

                for (i = 64; i < 69; i++) {
                    personaldataforBD[i + 44] = personaldata[i];
                }
                i = 69;

                if (personaldata[i] != null) {
                    if (personaldata[i].toLowerCase().equals("супруг") || personaldata[i].toLowerCase().equals("супруга") || personaldata[i].toLowerCase().equals("муж") || personaldata[i].toLowerCase().equals("жена")) {
                        personaldataforBD[113] = "1";
                        personaldataforBD[114] = "0";
                        personaldataforBD[115] = "0";
                        personaldataforBD[116] = "0";
                        personaldataforBD[117] = "0";
                        personaldataforBD[118] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("ребенок")) {
                        personaldataforBD[113] = "0";
                        personaldataforBD[114] = "1";
                        personaldataforBD[115] = personaldata[i];
                        personaldataforBD[116] = "0";
                        personaldataforBD[117] = "0";
                        personaldataforBD[118] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("внук") || personaldata[i].toLowerCase().equals("внучка")) {
                        personaldataforBD[113] = "1";
                        personaldataforBD[114] = "0";
                        personaldataforBD[115] = "0";
                        personaldataforBD[116] = "0";
                        personaldataforBD[117] = "0";
                        personaldataforBD[118] = "0";
                    }
                    if (personaldata[i].toLowerCase().equals("родственник по восходящей линии") || personaldata[i].toLowerCase().equals("иждивенец")) {
                        personaldataforBD[113] = "0";
                        personaldataforBD[114] = "0";
                        personaldataforBD[115] = "0";
                        personaldataforBD[116] = "0";
                        personaldataforBD[117] = "0";
                        personaldataforBD[118] = "1";
                    } else {
                        personaldataforBD[113] = "0";
                        personaldataforBD[114] = "0";
                        personaldataforBD[115] = "0";
                        personaldataforBD[116] = "0";
                        personaldataforBD[117] = "1";
                        personaldataforBD[118] = "0";
                    }
                }
                i++;
                //Toast.makeText(context, personaldata.length+" ", Toast.LENGTH_SHORT).show();

                for (i = 70; i < 87; i++) {
                    personaldataforBD[i + 50] = personaldata[i];
                }
                if (personaldata[70] != null && personaldata[71] != null) {
                    personaldataforBD[119] = personaldata[70] + " " + personaldata[71];
                }

                mResult = "[";
                for (String data : personaldata) {
                    if (data != null)
                        mResult += "\"" + data + "\",";
                    else
                        mResult += "\"\",";
                }
                mResult = mResult.substring(0, mResult.length() - 1) + "]";
            }
        });


        holder.btninfoaboutpersonaldata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pass the 'context' here
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Пояснение к " + personaldataanddescription[position]);
                alertDialog.setMessage(descriptionofpersonaldata[position]);
                alertDialog.setPositiveButton("Понятно", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.setNegativeButton("Больше информации", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        String kindarticle = "Полезные статьи";
                        Intent intent66 = new Intent(context, ArticlesActivity.class);
                        intent66.putExtra("activationOfAccount1", 1);
                        intent66.putExtra("kindarticle", kindarticle);
                        context.startActivity(intent66);
                    }
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });


    }


    @Override
    public int getItemCount() {
        return 87;
    }

    public String getResult() {
        return mResult;
    }

    public void setData(List<String> data) {
        for (int i = 0; i < data.size(); ++i)
            personaldata[i] = data.get(i);
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView Album;
        EditText AlbumTitle;
        TextView needdatatext;
        Button btninfoaboutpersonaldata, save;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            Album = itemView.findViewById(R.id.album);
            AlbumTitle = itemView.findViewById(R.id.album_title);
            needdatatext = itemView.findViewById(R.id.needdatatext);
            btninfoaboutpersonaldata = itemView.findViewById(R.id.btninfoaboutpersonaldata);
            save = itemView.findViewById(R.id.save);
        }


    }
}
